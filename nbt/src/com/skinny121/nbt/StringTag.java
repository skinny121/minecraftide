package com.skinny121.nbt;

import java.io.DataOutput;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 19/03/14
 * Time: 5:00 PM
 */
public class StringTag extends Tag {
    private String string;

    public StringTag(String name,String string) {
        super(name);
        this.string=string;
    }

    public String getString(){
        return string;
    }

    @Override
    public byte getID() {
        return 8;
    }

    @Override
    public void write(DataOutput output) throws IOException {
        output.writeUTF(string);
    }

    @Override
    public void write(NBTToJson.JsonBuilder builder, NBTToJson.Options options) {
        super.write(builder, options);
        builder.add('"');
        //add escapes
        String printing="";
        for(int i=0;i<string.length();i++){
            char ch=string.charAt(i);
            if(ch=='"'){
               printing+='\\';
            }
            printing+=ch;
        }
        builder.add(printing);
        builder.add('"');
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StringTag)) return false;

        StringTag stringTag = (StringTag) o;

        if (!string.equals(stringTag.string)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return string.hashCode();
    }
}
