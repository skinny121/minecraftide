package com.skinny121.nbt;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 20/03/14
 * Time: 3:14 PM
 */
public class NBTToJson {

    public static String toJson(Tag t, Options options){
        JsonBuilder builder=new JsonBuilder();
        t.write(builder,options);
        return builder.toString();
    }

    public static class JsonBuilder{
        private StringBuilder builder=new StringBuilder();
        private int indent=0;
        public void add(char c){
            builder.append(c);
        }

        public void add(String s){
            builder.append(s);
        }

        public void add(byte b){
            builder.append(b);
        }

        public void add(short s){
            builder.append(s);
        }

        public void add(int i){
            builder.append(i);
        }

        public void add(long l){
            builder.append(l);
        }

        public void add(float f){
            builder.append(f);
        }

        public void add(double d){
            builder.append(d);
        }

        public void newLine(){
            add('\n');
            for(int i=0;i<indent;i++){
                 add('\t');
            }
        }
        public void increaseIndent(){
            indent++;
        }
        public void decreaseIndent(){
            if(--indent<0)throw new IllegalStateException("indent is negative");
        }

        @Override
        public String toString() {
            return builder.toString();
        }
    }


    public static class Options{
        private boolean prettyPrinting;

        public Options(boolean prettyPrinting){
            this.prettyPrinting=prettyPrinting;
        }


        public boolean isPrettyPrinting(){
            return prettyPrinting;
        }
    }

}
