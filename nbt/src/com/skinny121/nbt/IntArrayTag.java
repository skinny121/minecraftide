package com.skinny121.nbt;

import java.io.DataOutput;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 19/03/14
 * Time: 5:36 PM
 */
public class IntArrayTag extends Tag {
    private int[] array;

    public IntArrayTag(String name,int[] array){
        super(name);
        this.array=array;
    }

    public int[] getIntArray(){
        return array;
    }

    @Override
    public byte getID() {
        return 11;
    }

    @Override
    public void write(DataOutput output) throws IOException {
        output.writeInt(array.length);
        for(int i:array){
            output.writeInt(i);
        }
    }

    @Override
    public void write(NBTToJson.JsonBuilder builder, NBTToJson.Options options) {
        super.write(builder, options);
        builder.add('[');
        int i=0;
        for(;i<array.length-1;i++){
            builder.add(array[i]);
            builder.add(",");
            if(options.isPrettyPrinting())
                builder.add(' ');
        }
        if(i<array.length){
            builder.add(array[i]);
        }
        builder.add(']');
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IntArrayTag)) return false;

        IntArrayTag that = (IntArrayTag) o;

        if (!Arrays.equals(array, that.array)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(array);
    }
}
