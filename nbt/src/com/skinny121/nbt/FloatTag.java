package com.skinny121.nbt;

import java.io.DataOutput;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 19/03/14
 * Time: 4:46 PM
 */
public class FloatTag extends Tag {
    private float f;

    public FloatTag(String name,float f) {
        super(name);
        this.f=f;
    }

    public float getFloat(){
        return f;
    }

    @Override
    public byte getID() {
        return 5;
    }

    @Override
    public void write(DataOutput output) throws IOException {
        output.writeFloat(f);
    }

    @Override
    public void write(NBTToJson.JsonBuilder builder, NBTToJson.Options options) {
        super.write(builder, options);
        builder.add(f);
        builder.add('f');
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FloatTag)) return false;

        FloatTag floatTag = (FloatTag) o;

        if (Float.compare(floatTag.f, f) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (f != +0.0f ? Float.floatToIntBits(f) : 0);
    }
}
