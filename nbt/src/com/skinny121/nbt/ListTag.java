package com.skinny121.nbt;

import java.io.DataOutput;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 19/03/14
 * Time: 5:12 PM
 */
public class ListTag<T extends Tag> extends Tag {
    private byte typeId;
    private T[] list;

    public ListTag(String name, T[] list,byte typeId) {
        super(name);
        this.list = list;
        this.typeId=typeId;
    }

    public T[] getList(){
        return list;
    }

    @Override
    public byte getID() {
        return 9;
    }

    @Override
    public void write(DataOutput output) throws IOException {
        output.writeByte(typeId);
        output.writeInt(list.length);
        for(T t:list){
            //write only payload
            t.write(output);
        }
    }

    @Override
    public void write(NBTToJson.JsonBuilder builder, NBTToJson.Options options) {
        super.write(builder, options);
        builder.add('[');
        int i=0;
        for(;i<list.length-1;i++){
            list[i].write(builder, options);
            builder.add(',');
            if(options.isPrettyPrinting()){
                if(typeId==10)
                    builder.newLine();//compound tag array
                else
                    builder.add(' ');
            }
        }
        if(i<list.length)
            list[i].write(builder, options);
        builder.add(']');
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ListTag)) return false;

        ListTag listTag = (ListTag) o;

        if (!Arrays.equals(list, listTag.list)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(list);
    }
}
