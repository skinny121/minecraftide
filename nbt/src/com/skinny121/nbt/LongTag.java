package com.skinny121.nbt;

import java.io.DataOutput;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 19/03/14
 * Time: 4:43 PM
 */
public class LongTag extends Tag {
    private long l;

    public LongTag(String name, long l) {
        super(name);
        this.l = l;
    }

    public long getLong(){
        return l;
    }

    @Override
    public byte getID() {
        return 4;
    }

    @Override
    public void write(DataOutput output) throws IOException {
        output.writeLong(l);
    }

    @Override
    public void write(NBTToJson.JsonBuilder builder, NBTToJson.Options options) {
        super.write(builder, options);
        builder.add(l);
        builder.add('l');
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LongTag)) return false;

        LongTag longTag = (LongTag) o;

        if (l != longTag.l) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (l ^ (l >>> 32));
    }
}
