package com.skinny121.nbt;

import java.io.*;
import java.util.zip.GZIPOutputStream;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 19/03/14
 * Time: 9:01 PM
 */
public class Exporter {

    public static void export(String filename,Tag tag){
        OutputStream out=null;
        try{
            File file=new File(filename);
            file.createNewFile();
            out=new FileOutputStream(file);
            export(out,tag);
        }catch(IOException e){
            e.printStackTrace();
        }finally {
            try{
                if (out != null) out.close();
            }catch(IOException e){
                e.printStackTrace();
            }
        }
    }

    public static void export(OutputStream out,Tag tag){
        GZIPOutputStream gzip=null;
        DataOutputStream data=null;
        try{
            gzip=new GZIPOutputStream(out);
            data=new DataOutputStream(gzip);
            //write tag
            data.writeByte(tag.getID());
            data.writeUTF(tag.getName());
            tag.write(data);
        }catch(IOException e){
            e.printStackTrace();
        }finally {
            try{
                if(data!=null)data.close();
                if (gzip != null) gzip.close();
                if (out != null) out.close();
            }catch(IOException e){
                e.printStackTrace();
            }
        }
    }
}
