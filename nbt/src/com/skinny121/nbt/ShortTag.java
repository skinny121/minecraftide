package com.skinny121.nbt;

import java.io.DataOutput;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 19/03/14
 * Time: 4:27 PM
 */
public class ShortTag extends Tag{
    private short s;

    public ShortTag(String name,short s) {
        super(name);
        this.s=s;
    }

    public short getShort(){
        return s;
    }

    @Override
    public byte getID() {
        return 2;
    }

    @Override
    public void write(DataOutput output) throws IOException {
        output.writeShort(s);
    }

    @Override
    public void write(NBTToJson.JsonBuilder builder, NBTToJson.Options options) {
        super.write(builder, options);
        builder.add(s);
        builder.add('s');
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ShortTag)) return false;

        ShortTag shortTag = (ShortTag) o;

        if (s != shortTag.s) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) s;
    }
}
