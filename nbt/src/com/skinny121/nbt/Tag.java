package com.skinny121.nbt;

import java.io.DataOutput;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 19/03/14
 * Time: 4:06 PM
 */
public abstract class Tag {
    private String name;

    public Tag(String name){
        this.name=name;
    }

    public String getName(){
        return name;
    }

    public abstract byte getID();

    public abstract void write(DataOutput output) throws IOException;

    public void write(NBTToJson.JsonBuilder builder,NBTToJson.Options options){
        if(name!=null){
            builder.add(name);
            builder.add(":");
        }
    }

    @Override
    public String toString() {
        return NBTToJson.toJson(this, new NBTToJson.Options(true));
    }
}
