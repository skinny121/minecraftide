package com.skinny121.nbt;

import java.io.DataOutput;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 19/03/14
 * Time: 4:54 PM
 */
public class ByteArrayTag extends Tag {
    private byte[] bytes;

    public ByteArrayTag(String name,byte[] bytes ) {
        super(name);
        this.bytes=bytes;
    }

    public byte[] getBytes(){
        return bytes;
    }

    @Override
    public byte getID() {
        return 7;
    }

    @Override
    public void write(DataOutput output) throws IOException {
        output.writeInt(bytes.length);
        output.write(bytes);
    }

    @Override
    public void write(NBTToJson.JsonBuilder builder,NBTToJson.Options options) {
        super.write(builder, options);
        builder.add('[');
        int i=0;
        for(;i<bytes.length-1;i++){
            builder.add(bytes[i]);
            builder.add("b,");
            if(options.isPrettyPrinting())
                builder.add(' ');
        }
        if(i<bytes.length){
            builder.add(bytes[i]);
            builder.add('b');
        }
        builder.add(']');
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ByteArrayTag)) return false;

        ByteArrayTag that = (ByteArrayTag) o;

        if (!Arrays.equals(bytes, that.bytes)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(bytes);
    }
}
