package com.skinny121.nbt;

import java.io.DataOutput;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 19/03/14
 * Time: 4:47 PM
 */
public class DoubleTag extends Tag {
    private double d;

    public DoubleTag(String name, double d) {
        super(name);
        this.d = d;
    }

    public double getDouble(){
        return d;
    }

    @Override
    public byte getID() {
        return 6;
    }

    @Override
    public void write(DataOutput output) throws IOException {
        output.writeDouble(d);
    }

    @Override
    public void write(NBTToJson.JsonBuilder builder, NBTToJson.Options options) {
        super.write(builder, options);
        builder.add(d);
        builder.add('d');
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DoubleTag)) return false;

        DoubleTag doubleTag = (DoubleTag) o;

        if (Double.compare(doubleTag.d, d) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        long temp = Double.doubleToLongBits(d);
        return (int) (temp ^ (temp >>> 32));
    }
}
