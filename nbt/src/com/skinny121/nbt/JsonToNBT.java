package com.skinny121.nbt;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 20/03/14
 * Time: 3:13 PM
 */
public class JsonToNBT {

    public static Tag toNBT(String s){
        return toNBT(s,Quotes.quotationRegions(s));
    }

    private static Tag toNBT(String s,Quotes quotes){
        int i=-1;
        do{
            i=s.indexOf(":",i+1);
        }while(quotes.inQuotes(i) && i!=-1);

        if(i==-1){
            return toNBT(null,s,quotes);
        }else {
            return toNBT(
                    s.substring(0,i).trim(),
                    s.substring(i+1),
                    quotes.moveIndex(i+1));
        }
    }

    private static Tag toNBT(String name,String value,Quotes quotes){
        char ch=value.charAt(0);
        value=value.trim();
        if(ch=='{'){
            Map<String,Tag> map=new TreeMap<String, Tag>();
            int endLocation= getCharacterIndex(value, quotes, '{', '}', 1);
            if(endLocation==-1)throw new RuntimeException("Unbalanced brackets");
            value=value.substring(1,endLocation);
            quotes=quotes.moveIndex(1);
            if(!value.isEmpty()){
                int i;
                do{
                    //get position of ,
                    i= getCharacterIndex(value,quotes,COMMA_PREDICATE_OPEN,COMMA_PREDICATE_CLOSE,getPredicate(','),0);
                    //get entry
                    String entry=i==-1 ? value : value.substring(0,i);
                    //create tag
                    Tag t=toNBT(entry,quotes);
                    map.put(t.getName(),t);
                    value=value.substring(i+1);
                    quotes=quotes.moveIndex(i+1);
                }while(i!=-1);
            }

            return new CompoundTag(name,map);
        }else if(ch=='['){
            List<Tag> list=new ArrayList<Tag>();
            int endLocation= getCharacterIndex(value, quotes, '[', ']', 1);
            if(endLocation==-1)throw new RuntimeException("Unbalanced brackets");
            value=value.substring(1,endLocation);
            quotes=quotes.moveIndex(1);
            if(!value.isEmpty()){
                int i;
                do{
                    //get position of ,
                    i= getCharacterIndex(value,quotes,COMMA_PREDICATE_OPEN,COMMA_PREDICATE_CLOSE,getPredicate(','),0);
                    //get entry
                    String entry= i==-1 ? value : value.substring(0,i);
                    //create tag
                    list.add(toNBT(null, entry, quotes));
                    value=value.substring(i+1);
                    quotes=quotes.moveIndex(i+1);
                }while(i!=-1);
            }
            byte tagId=list.isEmpty() ? 10 : list.get(0).getID();//default to compound
            switch(tagId){
                case 1:byte[] bytes=new byte[list.size()];
                       for(int i=0;i<bytes.length;i++){
                            bytes[i]= ((ByteTag) list.get(i)).getByte();
                       }
                       return new ByteArrayTag(name,bytes);
                case 3:int[] ints=new int[list.size()];
                       for(int i=0;i<ints.length;i++){
                          ints[i]= ((IntTag) list.get(i)).getInt();
                       }
                       return new IntArrayTag(name,ints);
                case 0:
                case 2:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                       return new ListTag<Tag>(name,list.toArray(new Tag[list.size()]),tagId);
                default:throw new RuntimeException("invalid tag id "+tagId);
            }
        }else{
            if(value.equals("null")) {
                return new CompoundTag(name, new HashMap<String, Tag>());
            }else if(value.equals("true")){
                return new ByteTag(name,(byte)1);
            }else if(value.equals("false")){
                return new ByteTag(name,(byte)0);
            }else if(value.matches("^(\\-|\\+)?[0-9]+b$")){
                return new ByteTag(name,Byte.parseByte(value.substring(0,value.length()-1)));
            }else if(value.matches("^(\\-|\\+)?[0-9]+s$")){
                return new ShortTag(name,Short.parseShort(value.substring(0,value.length()-1)));
            }else if(value.matches("^(\\-|\\+)?[0-9]+$")){
                return new IntTag(name,Integer.parseInt(value));
            }else if(value.matches("^(\\-|\\+)?[0-9]+l$")){
                return new LongTag(name,Long.parseLong(value.substring(0,value.length()-1)));
            }else if(value.matches("^(\\-|\\+)?[0-9]+\\.[0-9]+f$")){
                return new FloatTag(name,Float.parseFloat(value.substring(0,value.length()-1)));
            }else if(value.matches("^(\\-|\\+)?[0-9]+\\.[0-9]+d$")){
                return new DoubleTag(name,Double.parseDouble(value.substring(0,value.length()-1)));
            }else if(value.matches("^(\\-|\\+)?[0-9]+\\.[0-9]+$")){
                return new DoubleTag(name,Double.parseDouble(value));
            }else{
                if(value.charAt(0)=='"'){
                    if(value.charAt(value.length()-1)=='"'){
                        value=value.substring(1,value.length()-1);
                    }else{
                        throw new RuntimeException("cannot fins quotation mark");
                    }

                    //remove escaped characters
                    String result="";
                    boolean escaped=false;
                    for(int i=0;i<value.length();i++){
                        char ch1=value.charAt(i);
                        if(ch1=='"' && escaped){
                            result=result.substring(0,result.length()-1);
                        }
                        escaped=ch1=='\\';
                        result+=ch1;
                    }
                    value=result;
                }
                return new StringTag(name,value);
            }
        }
    }

    private static int getCharacterIndex(String s, Quotes quotes, char open, char close, int index){
        return getCharacterIndex(s,quotes,open,close,close,index);
    }

    private static int getCharacterIndex(String s, Quotes quotes, char open, char close, char locator, int index){
        return getCharacterIndex(s,quotes,getPredicate(open),getPredicate(close),getPredicate(locator),index);
    }

    private static int getCharacterIndex(String s, Quotes quotes, CharPredicate open, CharPredicate close, CharPredicate locator, int index){
        int depth=0;
        while(true){
            char ch=s.charAt(index);
            if(ch!='"' && !quotes.inQuotes(index)){
                if(locator.acceptChar(ch) && depth==0){
                    return index;
                }else if(open.acceptChar(ch)){
                    depth++;
                }else if(close.acceptChar(ch)){
                    depth--;
                }
            }

            index++;
            if(index>=s.length())
                return -1;
        }
    }

    private static final CharPredicate COMMA_PREDICATE_OPEN=new CharPredicate() {
        @Override
        public boolean acceptChar(char ch) {
            return ch=='[' || ch=='{';
        }
    };
    private static final CharPredicate COMMA_PREDICATE_CLOSE=new CharPredicate() {
        @Override
        public boolean acceptChar(char ch) {
            return ch==']' || ch=='}';
        }
    };

    private static CharPredicate getPredicate(final char ch){
        return new CharPredicate() {
            @Override
            public boolean acceptChar(char in) {
                return in==ch;
            }
        };
    }

    private static interface CharPredicate{
        boolean acceptChar(char ch);
    }
}
