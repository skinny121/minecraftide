package com.skinny121.nbt;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 19/03/14
 * Time: 4:01 PM
 */
public class Importer {
    private DataInputStream input;
    public static Tag importNBT(String str){
        InputStream in=null;
        Tag tag=null;
        try{
            File file=new File(str);
            in=new FileInputStream(file);
            tag=importNBT(in);
        }catch(IOException e){
            e.printStackTrace();
        }finally{
            try{
                if (in != null)in.close();
            }catch(IOException e){
                e.printStackTrace();
            }
        }
        return tag;
    }

    public static Tag importNBT(InputStream in){
        Importer importer=new Importer();
        GZIPInputStream gzip=null;
        Tag tag=null;
        try{
            gzip=new GZIPInputStream(in);
            importer.input=new DataInputStream(gzip);
            tag=importer.parseNBT(true);
        }catch(IOException e){
            e.printStackTrace();
        }finally{
            try{
                if (importer.input != null) importer.input.close();
                if (gzip != null)gzip.close();
                if (in != null)in.close();
            }catch(IOException e){
                e.printStackTrace();
            }
        }
        return tag;
    }

    public Tag parseNBT(boolean name) throws IOException{
        return parseNBT(name,get());
    }

    public Tag parseNBT(boolean name,byte id) throws IOException{
        String name2=name && id!=0 ? parseStringTag(null).getString() : null;
        switch(id){
            case 0:return parseEndTag();
            case 1:return parseByteTag(name2);
            case 2:return parseShortTag(name2);
            case 3:return parseIntTag(name2);
            case 4:return parseLongTag(name2);
            case 5:return parseFloatTag(name2);
            case 6:return parseDoubleTag(name2);
            case 7:return parseByteArrayTag(name2);
            case 8:return parseStringTag(name2);
            case 9:return parseListTag(name2);
            case 10:return parseCompoundTag(name2);
            case 11:return parseIntArrayTag(name2);
            default:
                throw new IllegalStateException("invalid type "+id);
        }
    }

    private EndTag parseEndTag() throws IOException{
        return new EndTag();
    }

    private ByteTag parseByteTag(String name) throws IOException{
        return new ByteTag(name,get());
    }

    private ShortTag parseShortTag(String name) throws IOException{
        return new ShortTag(name,input.readShort());
    }

    private IntTag parseIntTag(String name) throws IOException{
        return new IntTag(name,input.readInt());
    }

    private LongTag parseLongTag(String name) throws IOException{
        return new LongTag(name,input.readLong());
    }

    private FloatTag parseFloatTag(String name) throws IOException{
        return new FloatTag(
                name,
                input.readFloat());
    }

    private DoubleTag parseDoubleTag(String name) throws IOException{
        return new DoubleTag(
                name,
                input.readDouble());
    }

    private ByteArrayTag parseByteArrayTag(String name) throws IOException{
        byte[] result=new byte[input.readInt()];
        input.readFully(result);
        return new ByteArrayTag(name,result);
    }

    private IntArrayTag parseIntArrayTag(String name) throws IOException{
        int[] result=new int[input.readInt()];
        for(int i=0;i<result.length;i++){
            result[i]=parseIntTag(null).getInt();
        }
        return new IntArrayTag(name,result);
    }

    private StringTag parseStringTag(String name) throws IOException{
        return new StringTag(name,input.readUTF());
    }

    private ListTag parseListTag(String name) throws IOException{
        byte id=get();
        int length=parseIntTag(null).getInt();
        Tag[] result=new Tag[length];
        for(int i=0;i<length;i++){
            result[i]=parseNBT(false,id);
        }
        return new ListTag<Tag>(name,result,id);
    }

    private CompoundTag parseCompoundTag(String name) throws IOException{
        Map<String,Tag> map=new HashMap<String, Tag>();
        Tag t=parseNBT(true);
        while(!(t instanceof EndTag)){
            map.put(t.getName(),t);
            t=parseNBT(true);
        }
        return new CompoundTag(name,map);
    }

    private byte get() throws IOException{
        return input.readByte();
    }



}
