package com.skinny121.nbt;

import java.io.DataOutput;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 19/03/14
 * Time: 5:18 PM
 */
public class CompoundTag extends Tag {
    private Map<String,Tag> map;

    public CompoundTag(String name, Map<String, Tag> map) {
        super(name);
        this.map = map;
    }

    public Tag getTag(String name){
        return map.get(name);
    }

    public Map<String,Tag> getMap(){
        return map;
    }

    @Override
    public byte getID() {
        return 10;
    }

    @Override
    public void write(DataOutput output) throws IOException {
        for(Tag tag:getMap().values()){
            //type
            output.writeByte(tag.getID());
            //name
            output.writeUTF(tag.getName());
            //payload
            tag.write(output);
        }
        //tag end
        output.writeByte(0);
    }

    @Override
    public void write(NBTToJson.JsonBuilder builder, NBTToJson.Options options) {
        super.write(builder, options);

        Iterator<Tag> iterator= map.values().iterator();

        builder.add('{');
        builder.increaseIndent();

        while(iterator.hasNext()){
            if(options.isPrettyPrinting()){
                builder.newLine();
            }
            iterator.next().write(builder, options);
            if(iterator.hasNext()){
                builder.add(',');
            }
        }

        builder.decreaseIndent();
        if(options.isPrettyPrinting() && map.size()>0){
            builder.newLine();
        }
        builder.add('}');
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CompoundTag)) return false;

        CompoundTag that = (CompoundTag) o;

        if (!map.equals(that.map)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return map.hashCode();
    }
}
