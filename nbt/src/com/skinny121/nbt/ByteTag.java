package com.skinny121.nbt;

import java.io.DataOutput;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 19/03/14
 * Time: 4:23 PM
 */
public class ByteTag extends Tag {
    private byte b;
    public ByteTag(String name,byte b) {
        super(name);
        this.b=b;
    }

    public byte getByte(){
        return b;
    }

    @Override
    public byte getID() {
        return 1;
    }

    @Override
    public void write(DataOutput output) throws IOException {
        output.writeByte(b);
    }

    @Override
    public void write(NBTToJson.JsonBuilder builder, NBTToJson.Options options) {
        super.write(builder, options);
        builder.add(b);
        builder.add('b');
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ByteTag)) return false;

        ByteTag byteTag = (ByteTag) o;

        if (b != byteTag.b) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) b;
    }
}
