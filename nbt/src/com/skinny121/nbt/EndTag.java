package com.skinny121.nbt;

import java.io.DataOutput;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 19/03/14
 * Time: 4:14 PM
 */
public class EndTag extends Tag{
    public EndTag() {
        super(null);
    }

    @Override
    public byte getID() {
        return 0;
    }

    @Override
    public void write(DataOutput output) throws IOException {
    }

    @Override
    public void write(NBTToJson.JsonBuilder builder, NBTToJson.Options options) {}
}
