package com.skinny121.nbt;

import java.util.Arrays;

/**
* Created with IntelliJ IDEA.
* User: BenLewis
* Date: 21/03/14
* Time: 9:30 PM
*/
class Quotes{

    protected int[] quotes;
    protected int index;
    protected Quotes(int[] quotes, int index){
        this.quotes = quotes;
        this.index=index;
    }

    boolean inQuotes(int i){
        return inQuotes(index + i, 0, quotes.length + 1);
    }

    private boolean inQuotes(int index, int begin, int end){
        int mid=(begin+end)/2;
        if(mid!=end-1 && index> quotes[mid]){
            return inQuotes(index, mid + 1, end);
        }else if(mid != 0 && index < quotes[mid - 1]){
            return inQuotes(index, begin, mid);
        }else{
            //check to see if index is a quotation mark
            if((mid!=end-1 && quotes[mid]==index)
                    || (mid != 0 && quotes[mid-1]==index)){
                throw new RuntimeException("Is separator");
            }
            return mid%2==1;
        }
    }

    static Quotes quotationRegions(String s){
        boolean escape=false;
        int[] array=new int[s.length()];
        int counter=0;
        for(int i=0;i<s.length();i++){
            char ch=s.charAt(i);
            if(ch=='"'){
                if(!escape){
                    array[counter++]=i;
                }
            }
            escape = ch == '\\';
        }
        if(counter%2==1) {
            throw new RuntimeException("Unbalanced quotations");
        }
        return new Quotes(Arrays.copyOf(array, counter),0);
    }

    Quotes moveIndex(int i) {
        return new Quotes(quotes,index+i);
    }
}
