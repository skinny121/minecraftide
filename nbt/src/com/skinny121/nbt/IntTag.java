package com.skinny121.nbt;

import java.io.DataOutput;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 19/03/14
 * Time: 4:38 PM
 */
public class IntTag extends Tag {

    private int i;

    public IntTag(String name,int i) {
        super(name);
        this.i=i;
    }

    public int getInt(){
        return i;
    }

    @Override
    public byte getID() {
        return 3;
    }

    @Override
    public void write(DataOutput output) throws IOException {
        output.writeInt(i);
    }

    @Override
    public void write(NBTToJson.JsonBuilder builder, NBTToJson.Options options) {
        super.write(builder, options);
        builder.add(i);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IntTag)) return false;

        IntTag intTag = (IntTag) o;

        if (i != intTag.i) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return i;
    }
}
