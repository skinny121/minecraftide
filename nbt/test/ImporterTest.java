import com.skinny121.nbt.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 19/03/14
 * Time: 6:37 PM
 */
public class ImporterTest {

    @Test
    public void importExport(){
        String file="/Users/BenLewis/test.nbt";


        //setup test nbt data
        Map<String,Tag> map=new TreeMap<String, Tag>();

        map.put("byte",new ByteTag("byte",(byte)3));
        map.put("short",new ShortTag("short",(short)-127));
        map.put("int",new IntTag("int",0x4ffff));
        map.put("long",new LongTag("long",0xffffee22<<8));
        map.put("float",new FloatTag("float",7.23f));
        map.put("double",new DoubleTag("double",8.95));

        map.put("string",new StringTag("string","Hi there"));

        map.put("byteArray",new ByteArrayTag("byteArray",new byte[]{4,3,7,5}));
        map.put("intArray",new IntArrayTag("intArray",new int[]{4,314,700,5}));

        StringTag[] stringTags=new StringTag[5];
        stringTags[0]=new StringTag(null,"a");
        stringTags[1]=new StringTag(null,"b");
        stringTags[2]=new StringTag(null,"c");
        stringTags[3]=new StringTag(null,"d");
        stringTags[4]=new StringTag(null,"d\"");
        map.put("list",new ListTag<StringTag>("list",stringTags,stringTags[0].getID()));

        CompoundTag root=new CompoundTag("root",map);

        Exporter.export(file,root);

        Assert.assertEquals("Binary Conversion",root,Importer.importNBT(file));
        //System.out.println(NBTToJson.toJson(root, new NBTToJson.Options(false)));
        System.out.println(NBTToJson.toJson(root, new NBTToJson.Options(true)));

        //json
        String json=NBTToJson.toJson(root, new NBTToJson.Options(false));
        Assert.assertEquals("Json Conversion",root,JsonToNBT.toNBT(json));

        json=NBTToJson.toJson(root, new NBTToJson.Options(true));
        Assert.assertEquals("Json Conversion Pretty Printing",root,JsonToNBT.toNBT(json));


    }

    @Test
    public void levelImport(){
        String file="/Users/BenLewis/Library/Application Support/minecraft/1.8/saves/CheckPoint/level.dat";
        Tag t=Importer.importNBT(file);

        System.out.println(NBTToJson.toJson(t, new NBTToJson.Options(true)));
    }

    @Test
    public void schemImport(){
        String file="/Users/BenLewis/Documents/MCEdit-schematics/TileTickTest.schematic";
        Tag t=Importer.importNBT(file);

        System.out.println(NBTToJson.toJson(t, new NBTToJson.Options(true)));
    }

}
