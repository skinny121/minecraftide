package com.skinny121.nbt;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 21/03/14
 * Time: 9:34 PM
 */
@RunWith(JUnit4.class)
public class QuoteTest {

    @Rule
    public ExpectedException expectedException=ExpectedException.none();

    @Test
    public void testQuote(){
        Quotes quotes=Quotes.quotationRegions("0\"23\"5");
        Assert.assertTrue(!quotes.inQuotes(0));
        Assert.assertTrue(!quotes.inQuotes(5));

        Assert.assertTrue(quotes.inQuotes(2));
        Assert.assertTrue(quotes.inQuotes(3));

        expectedException.expect(RuntimeException.class);
        expectedException.expectMessage("Is separator");
        quotes.inQuotes(1);
    }
    @Test
    public void testQuoteEscaped(){
        Quotes.quotationRegions("a\\\"a");
    }

    @Test
    public void testQuoteUnbalanced(){
        expectedException.expect(RuntimeException.class);
        expectedException.expectMessage("Unbalanced quotations");
        Quotes.quotationRegions("a\"a");
    }

}
