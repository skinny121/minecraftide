package com.skinny121.mcide.impl.tt;

import com.skinny121.mcide.Token;
import org.junit.Assert;
import org.junit.Test;
import static com.skinny121.mcide.impl.tt.TokenTree.*;
/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 16/08/14
 * Time: 3:29 PM
 */
public class TTTest {
    @Test
    public void orTest(){
        TokenTree tree=charTree('a').name("A").or(charTree('b').name("B"));
        CompiledTree compiled=tree.build();

        Token t=compiled.createToken("a");
        Assert.assertEquals("A",t.getTokenType());
        Assert.assertEquals(1,t.getLength());

        t=compiled.createToken("b");
        Assert.assertEquals("B",t.getTokenType());
        Assert.assertEquals(1,t.getLength());

        Assert.assertNull(compiled.createToken("c"));
    }

    @Test
    public void mayTest(){
        TokenTree tree=charTree('a').may(charTree('b')).name("Token");
        CompiledTree compiled=tree.build();

        Token t;
        t=compiled.createToken("a");
        Assert.assertEquals("Token",t.getTokenType());
        Assert.assertEquals(1,t.getLength());

        t=compiled.createToken("ab");
        Assert.assertEquals("Token",t.getTokenType());
        Assert.assertEquals(2,t.getLength());

        t=compiled.createToken("abc");
        Assert.assertEquals("Token",t.getTokenType());
        Assert.assertEquals(2,t.getLength());

        t=compiled.createToken("acb");
        Assert.assertEquals("Token",t.getTokenType());
        Assert.assertEquals(1,t.getLength());
    }

    @Test
    public void thenTest(){
        TokenTree tree=charTree('a').then(charTree('b')).name("Token");
        CompiledTree compiled=tree.build();

        Token t;
        t=compiled.createToken("a");
        Assert.assertEquals("Part:Token",t.getTokenType());
        Assert.assertEquals(1,t.getLength());

        t=compiled.createToken("ab");
        Assert.assertEquals("Token",t.getTokenType());
        Assert.assertEquals(2,t.getLength());

        t=compiled.createToken("abc");
        Assert.assertEquals("Token",t.getTokenType());
        Assert.assertEquals(2,t.getLength());

        t=compiled.createToken("acb");
        Assert.assertEquals("Part:Token",t.getTokenType());
        Assert.assertEquals(1,t.getLength());
    }

    @Test
    public void defaultNameTest(){
        TokenTree tree=charTree('a').name("A").or(charTree('b').name("B")).defaultTo("Default");
        CompiledTree compiled=tree.build();

        Token t;
        t=compiled.createToken("a");
        Assert.assertEquals("A",t.getTokenType());
        Assert.assertEquals(1,t.getLength());

        t=compiled.createToken("b");
        Assert.assertEquals("B",t.getTokenType());
        Assert.assertEquals(1,t.getLength());

        t=compiled.createToken("xyz");
        Assert.assertEquals("Default",t.getTokenType());
        Assert.assertEquals(0,t.getLength());
    }
    @Test
    public void defaultTest(){
        TokenTree tree=charTree('a').name("A").or(charTree('b').name("B")).defaultTo(charTree('c').then(charTree('c')).name("C"));
        CompiledTree compiled=tree.build();

        Token t;
        t=compiled.createToken("a");
        Assert.assertEquals("A",t.getTokenType());
        Assert.assertEquals(1,t.getLength());

        t=compiled.createToken("b");
        Assert.assertEquals("B",t.getTokenType());
        Assert.assertEquals(1,t.getLength());

        t=compiled.createToken("c");
        Assert.assertEquals("Part:C",t.getTokenType());
        Assert.assertEquals(1,t.getLength());

        t=compiled.createToken("cc");
        Assert.assertEquals("C",t.getTokenType());
        Assert.assertEquals(2,t.getLength());
    }

    @Test
    public void repeatTest(){
        TokenTree tree=charTree('a').mayRepeat().name("A");
        CompiledTree compiled=tree.build();

        Token t;
        t=compiled.createToken("a");
        Assert.assertEquals("A",t.getTokenType());
        Assert.assertEquals(1,t.getLength());

        t=compiled.createToken("aa");
        Assert.assertEquals("A",t.getTokenType());
        Assert.assertEquals(2,t.getLength());

        t=compiled.createToken("aab");
        Assert.assertEquals("A",t.getTokenType());
        Assert.assertEquals(2,t.getLength());
    }

    @Test
    public void charRangeTest(){
        TokenTree tree=charRange("A",'a','c');
        CompiledTree compiled=tree.build();

        Token t;
        t=compiled.createToken("a");
        Assert.assertEquals("A",t.getTokenType());
        Assert.assertEquals(1,t.getLength());

        t=compiled.createToken("b");
        Assert.assertEquals("A",t.getTokenType());
        Assert.assertEquals(1,t.getLength());

        t=compiled.createToken("c");
        Assert.assertEquals("A",t.getTokenType());
        Assert.assertEquals(1,t.getLength());

        t=compiled.createToken("d");
        Assert.assertNull(t);
    }

    @Test
    public void wordTest(){
        TokenTree tree=match("Hello").name("A");
        CompiledTree compiled=tree.build();

        Token t;
        t=compiled.createToken("Hello");
        Assert.assertEquals("A",t.getTokenType());
        Assert.assertEquals(5,t.getLength());

        t=compiled.createToken("Hello!");
        Assert.assertEquals("A",t.getTokenType());
        Assert.assertEquals(5,t.getLength());

        t=compiled.createToken("Hel");
        Assert.assertEquals("Part:A",t.getTokenType());
        Assert.assertEquals(3,t.getLength());
    }
}
