package com.skinny121.mcide.impl;

import com.skinny121.mcide.TokenContext;

import java.util.List;

/**
* Created with IntelliJ IDEA.
* User: BenLewis
* Date: 6/07/14
* Time: 3:05 PM
*/
public interface CollectionGen<T,R>{
    R create(TokenContext tokens, List<T> list);
}
