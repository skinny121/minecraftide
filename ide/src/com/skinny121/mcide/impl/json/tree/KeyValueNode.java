package com.skinny121.mcide.impl.json.tree;

import com.skinny121.mcide.TokenContext;
import com.skinny121.mcide.tree.ASTNode;
import com.skinny121.mcide.tree.InternalNode;

import java.util.stream.Stream;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 6/07/14
 * Time: 2:51 PM
 */
public class KeyValueNode extends InternalNode {
    private StringNode key;
    private ASTNode value;

    public KeyValueNode(TokenContext tokenContext,StringNode key,ASTNode value) {
        super(tokenContext);
        this.key=key;
        this.value=value;
    }

    public StringNode getKey() {
        return key;
    }

    public ASTNode getValue() {
        return value;
    }

    @Override
    public Stream<? extends ASTNode> getChildren() {
        return Stream.of(key,value);
    }
}
