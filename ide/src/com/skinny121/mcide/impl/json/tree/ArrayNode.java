package com.skinny121.mcide.impl.json.tree;

import com.skinny121.mcide.TokenContext;
import com.skinny121.mcide.tree.ASTNode;
import com.skinny121.mcide.tree.InternalNode;

import java.util.List;
import java.util.stream.Stream;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 6/07/14
 * Time: 9:21 AM
 */
public class ArrayNode extends InternalNode {
    private List<ASTNode> children;
    public ArrayNode(TokenContext tokenContext,List<ASTNode> children) {
        super(tokenContext);
        this.children=children;
    }

    @Override
    public Stream<? extends ASTNode> getChildren() {
        return children.stream();
    }
}
