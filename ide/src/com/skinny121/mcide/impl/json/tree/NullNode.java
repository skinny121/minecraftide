package com.skinny121.mcide.impl.json.tree;

import com.skinny121.mcide.TokenContext;
import com.skinny121.mcide.tree.ASTNode;
import com.skinny121.mcide.tree.LeafNode;

import java.util.stream.Stream;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 6/07/14
 * Time: 12:14 PM
 */
public class NullNode extends LeafNode {

    public NullNode(TokenContext tokenContext) {
        super(tokenContext);
    }

    @Override
    public Stream<? extends ASTNode> getChildren() {
        return Stream.empty();
    }

    @Override
    public String getStyle() {
        return "null";
    }
}
