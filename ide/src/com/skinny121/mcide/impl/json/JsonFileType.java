package com.skinny121.mcide.impl.json;

import com.skinny121.mcide.*;
import com.skinny121.mcide.TokenContext.Builder;
import com.skinny121.mcide.impl.*;
import com.skinny121.mcide.impl.json.tree.*;
import com.skinny121.mcide.tree.ASTNode;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 4/07/14
 * Time: 1:46 PM
 */
public class JsonFileType implements FileType {

    private static JsonFileType fileType;

    private static TokenRegister tokenRegister=new TokenRegister();
    public static final TokenType
            LEFT_BRACE,RIGHT_BRACE,
            LEFT_BRACKET,RIGHT_BRACKET,
            COMMA,
            COLON,
            QUOTATION,
            DOT,
            SLASH,
            BOOLEAN,
            NULL,
            SIGN,
            EXP,
            NUMBER,
            SPACE,
            STRING;
    static{
        LEFT_BRACE=tokenRegister.register(new CharTokenType('{'));
        RIGHT_BRACE=tokenRegister.register(new CharTokenType('}'));
        LEFT_BRACKET=tokenRegister.register(new CharTokenType('['));
        RIGHT_BRACKET=tokenRegister.register(new CharTokenType(']'));
        COMMA=tokenRegister.register(new CharTokenType(','));
        COLON=tokenRegister.register(new CharTokenType(':'));
        QUOTATION=tokenRegister.register(new CharTokenType('"'));
        DOT=tokenRegister.register(new CharTokenType('.'));
        SLASH=tokenRegister.register(new CharTokenType('\\'));
        BOOLEAN=tokenRegister.register(new RegexTokenType("boolean","true|false"));
        NULL=tokenRegister.register(new RegexTokenType("null","null"));
        SIGN=tokenRegister.register(new RegexTokenType("sign","\\-|\\+"));
        EXP=tokenRegister.register(new RegexTokenType("exp","e|E"));

        NUMBER=tokenRegister.register(new RegexTokenType("number","[\\-\\+]?[0-9]+(\\.[0-9]+)?([eE][\\-\\+]?[0-9]+)?"));
        tokenRegister.register(new RegexTokenType("number:part-num.","[\\-\\+]?[0-9]+\\."));
        tokenRegister.register(new RegexTokenType("number:part-.num","\\.[0-9]+([eE][\\-\\+]?[0-9]*)?"));
        tokenRegister.register(new RegexTokenType("number:part-exp_no_digits","[\\-\\+]?[0-9]+(\\.[0-9]+)?([eE][\\-\\+]?)?"));

        SPACE=tokenRegister.register(new RegexTokenType("space","[\n\r\u0009\u0020]+"));

        STRING=tokenRegister.registerLast(new RegexTokenType("string","[^{}\\[\\],:\\.\\\\eE0-9\\-\\+\n\r\u0009\u0020]+"));
    }

    public static JsonFileType getInstance(){
        if (fileType == null) {
            fileType = new JsonFileType();
        }
        return fileType;
    }

    private ErrorHandler errorHandler=new DefaultErrorHandler();

    @Override
    public boolean accept(String name) {
        return name.endsWith(".json");
    }

    @Override
    public Lexer lex(String text) {
        return new TokenLexer(tokenRegister,text);
    }

    @Override
    public ASTNode parse(Lexer tokens) {
        TokenContext context=tokens.createTokenContext();
        context.filter(t->!SPACE.isOf(t));
        return parseValue(context);
    }

    private ASTNode parseValue(TokenContext tokens){
        if(LEFT_BRACE.isOf(tokens.token())){
            return parseObject(tokens);
        }else if(LEFT_BRACKET.isOf(tokens.token())) {
            return parseArray(tokens);
        }else if(QUOTATION.isOf(tokens.token())){
            return parseString(tokens);
        }else {
            Node start=tokens.node();
            TokenContext context=tokens.subContext();
            String value=context.getString(start);
            tokens.nextToken();
            if(NUMBER.isOf(start.getToken())){
                return new NumberNode(context,value);
            }else if(BOOLEAN.isOf(start.getToken())){
                return new BooleanNode(context,value.equals("true"));
            }else if(NULL.isOf(start.getToken())){
                return new NullNode(context);
            }else{
                errorHandler.handleError(ErrorLevel.ERROR,"isn't value",context);
                return null;
            }
        }
    }

    private KeyValueNode parseKeyValue(TokenContext tokens){
        Builder builder=tokens.builder();
        StringNode str=parseKey(tokens);
        if(!COLON.isOf(tokens.token())){
            errorHandler.handleError(ErrorLevel.ERROR,"expected :",tokens.node());
        }
        tokens.nextToken();
        ASTNode value=parseValue(tokens);
        return new KeyValueNode(builder.build(),str,value);
    }

    private StringNode parseKey(TokenContext tokens){
        if(QUOTATION.isOf(tokens.token())){
            return parseString(tokens);
        }else{
            errorHandler.handleError(ErrorLevel.ERROR, "expected key", tokens.node());
            return null;
        }
    }

    private <T,R> R parseCollection(TokenContext tokens,
                                     Function<TokenContext,T> function,
                                     Consumer<Node> check,
                                     CollectionGen<T,R> gen){
        ArrayList<T> list=new ArrayList<>();
        Builder builder=tokens.builder();
        do{
            tokens.nextToken();
            list.add(function.apply(tokens));
        }while(COMMA.isOf(tokens.token()));
        check.accept(tokens.node());
        tokens.nextToken();
        return gen.create(builder.build(), list);
    }
    private ObjectNode parseObject(TokenContext tokens){
        return parseCollection(tokens,this::parseKeyValue,this::checkObject,ObjectNode::new);
    }

    private void checkObject(Node node){
        if(!RIGHT_BRACE.isOf(node.getToken())){
            errorHandler.handleError(ErrorLevel.ERROR,"expected }",node);
        }
    }

    private ArrayNode parseArray(TokenContext tokens){
        return parseCollection(tokens,this::parseValue,this::checkArray,ArrayNode::new);
    }

    private void checkArray(Node node){
        if(!RIGHT_BRACKET.isOf(node.getToken())){
            errorHandler.handleError(ErrorLevel.ERROR,"expected ]",node);
        }
    }

    private StringNode parseString(TokenContext tokens) {
        Builder builder=tokens.builder();
        Node n=tokens.nextNode();
        String result="";
        while (!QUOTATION.isOf(n.getToken())){
            result+=processString(tokens.getString(n));
            n=tokens.nextNode();
            if(SLASH.isOf(n.getToken())){
                tokens.nextToken();
                result+=processStringEscaped(tokens);
                n=tokens.nextNode();
            }
        }
        tokens.nextToken();
        return new StringNode(builder.build(),result);
    }
    private String processString(String raw){
        String result="";
        for(int i=0;i<raw.length();i++){
            char ch=raw.charAt(i);
            if(ch>0 && ch<=0x1F){
                errorHandler.handleError(ErrorLevel.ERROR,"illegal control character",i,i++);
            }else{
                result+=ch;
            }
        }
        return result;
    }

    private String processStringEscaped(TokenContext tokens){
        String result="";
        Counter counter=new Counter(1);
        char ch=tokens.getString(tokens.node()).charAt(0);
        switch (ch){
            case '"':
            case '\\':
            case '/':
                result+=ch;
                break;
            case 'b':
                result+='\b';
                break;
            case 'f':
                result+='\f';
                break;
            case 'n':
                result+='\n';
                break;
            case 'r':
                result+='\r';
                break;
            case 't':
                result+='\t';
                break;
            case 'u':
                String hex="";
                hex+=checkCodePoint(counter,tokens);
                hex+=checkCodePoint(counter,tokens);
                hex+=checkCodePoint(counter,tokens);
                hex+=checkCodePoint(counter,tokens);
                result+=(char)Integer.parseInt(hex, 16);
                break;
        }
        return result+processString(tokens.getString(tokens.node()).substring(counter.counter));
    }

    private char checkCodePoint(Counter counter,TokenContext tokens){
        Node n=tokens.node();
        if(counter.counter>=tokens.getString(n).length()){
            n=tokens.nextNode();
            counter.counter=0;
        }
        char ch=tokens.getString(n).charAt(counter.counter);
        if((ch>='0' && ch<='9') || (ch>='a' && ch<='f') || (ch>='A' && ch<='F')){
            counter.inc();
            return ch;
        }else{
            counter.inc();
            int pos=tokens.node().getStartStringIndex()+counter.counter-1;
            errorHandler.handleError(ErrorLevel.ERROR,"isn't a valid hex character",pos,pos+1);
            return '0';
        }
    }

    @Override
    public String decode(InputStream in) throws IOException {
        return IOUtils.toString(in,"UTF-8");
    }

    @Override
    public void encode(String text, OutputStream out) throws IOException {
        IOUtils.write(text,out);
    }

    @Override
    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler=errorHandler;
    }

    private class Counter{
        private int counter;
        private Counter(int counter){
            this.counter=counter;
        }

        private void inc(){
            counter++;
        }
    }

}
