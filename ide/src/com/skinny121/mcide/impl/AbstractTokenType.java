package com.skinny121.mcide.impl;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 12/07/14
 * Time: 9:16 PM
 */
public abstract class AbstractTokenType implements TokenType {
    private List<TokenType> dependencies;
    private String name;
    protected AbstractTokenType(String name,List<TokenType> dependencies) {
        this.name=name;
        this.dependencies = dependencies;
    }

    @Override
    public boolean canTry(List<TokenType> attempted) {
        return attempted.containsAll(dependencies);
    }

    @Override
    public String getTokenName() {
        return name;
    }
}
