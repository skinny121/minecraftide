package com.skinny121.mcide.impl;

import com.skinny121.mcide.Lexer;
import com.skinny121.mcide.LexerEventDelete;
import com.skinny121.mcide.LexerEventInsert;
import com.skinny121.mcide.LexerListener;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 4/07/14
 * Time: 2:36 PM
 */
public abstract class AbstractLexer implements Lexer {
    private Set<LexerListener<LexerEventInsert>> insertListeners=new HashSet<>();
    private Set<LexerListener<LexerEventDelete>> deleteListeners=new HashSet<>();

    protected void notifyInsert(LexerEventInsert event){
        insertListeners.forEach(l->l.lexerChanged(event));
    }

    @Override
    public void addLexerInsertListener(LexerListener<LexerEventInsert> listener) {
        insertListeners.add(listener);
    }

    @Override
    public void removeLexerInsertListener(LexerListener<LexerEventInsert> listener) {
        insertListeners.remove(listener);
    }

    protected void notifyDelete(LexerEventDelete event){
        deleteListeners.forEach(l->l.lexerChanged(event));
    }

    @Override
    public void addLexerDeleteListener(LexerListener<LexerEventDelete> listener) {
        deleteListeners.add(listener);
    }

    @Override
    public void removeLexerDeleteListener(LexerListener<LexerEventDelete> listener) {
        deleteListeners.remove(listener);
    }
}
