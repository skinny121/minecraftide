package com.skinny121.mcide.impl;

import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 10/07/14
 * Time: 1:48 PM
 */
public class RegexTokenType extends AbstractTokenType {
    private Pattern regex;

    public RegexTokenType(String name,List<TokenType> dependencies,String regex){
        super(name,dependencies);
        this.regex=Pattern.compile(regex);
    }

    public RegexTokenType(String name,String regex){
        this(name, Collections.emptyList(),regex);
    }

    @Override
    public int createToken(String s) {
        Matcher matcher=regex.matcher(s);
        if(matcher.lookingAt()){
            return matcher.end();
        }else{
            return -1;
        }
    }
}
