package com.skinny121.mcide.impl;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 9/07/14
 * Time: 6:17 PM
 */
public class TokenRegister {

    private Map<String,TokenType> tokenTypes=new HashMap<>();
    private List<TokenType> tokenTypeList=new ArrayList<>();
    private TokenType last;

    public synchronized TokenType registerLast(TokenType type){
        tokenTypes.put(type.getTokenName(),type);
        this.last=type;
        return type;
    }

    public synchronized TokenType register(TokenType type){
        tokenTypes.put(type.getTokenName(),type);
        tokenTypeList.add(type);
        return type;
    }

    public synchronized List<TokenType> getTokenTypes(){
        int size=tokenTypeList.size();
        int i=0;
        List<TokenType> attempted=new ArrayList<>();
        while(attempted.size()<size){
            TokenType tokenType=tokenTypeList.get(i);
            if(!attempted.contains(tokenType) && tokenType.canTry(attempted)){
                attempted.add(tokenType);
            }
            i++;
            if(i>=size)i=0;
        }
        attempted.add(last);
        return attempted;
    }

    public TokenType getTokenType(String s){
        return tokenTypes.get(s);
    }
}
