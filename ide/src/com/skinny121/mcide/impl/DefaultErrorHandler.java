package com.skinny121.mcide.impl;

import com.skinny121.mcide.ErrorHandler;
import com.skinny121.mcide.ErrorLevel;
import com.skinny121.mcide.Token;
import com.skinny121.mcide.TokenContext;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 6/07/14
 * Time: 5:30 PM
 */
public class DefaultErrorHandler implements ErrorHandler {
    @Override
    public void handleError(ErrorLevel level, String message) {

    }

    @Override
    public void handleError(ErrorLevel level, String message, int start, int end) {

    }
}
