package com.skinny121.mcide.impl;

import com.skinny121.mcide.Token;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 9/07/14
 * Time: 6:18 PM
 */
public interface TokenType {

    boolean canTry(List<TokenType> attempted);

    int createToken(String s);

    String getTokenName();

    default boolean isOf(Token t){
       return t.getTokenType().endsWith(getTokenName());
    }
}
