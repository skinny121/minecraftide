package com.skinny121.mcide.impl.tt;

import java.util.ArrayList;
import java.util.List;

/**
* Created with IntelliJ IDEA.
* User: BenLewis
* Date: 27/08/14
* Time: 12:44 PM
*/
class RepeatOp extends CharOp{
    private CharOp treeA;
    private List<CharOp> list;
    RepeatOp(TokenTree tree, String name) {
        super(name);
        treeA= tree.assemble(name());
        treeA.entries().setDefault(new ResultOp(name()));

        list=new ArrayList<>();
        list.add(this);
    }

    @Override
    public CharArray<CharOp> entries() {
        return treeA.entries();
    }

    @Override
    List<CharOp> next() {
        return list;
    }
}
