package com.skinny121.mcide.impl.tt;

/**
* Created with IntelliJ IDEA.
* User: BenLewis
* Date: 25/08/14
* Time: 1:59 PM
*/
class ResultOp extends CharOp{
    boolean prev;

    ResultOp(String name) {
        this(name,false);
    }

    ResultOp(String name,boolean prev){
        super(name);
        this.prev=prev;
    }
    @Override
    CharArray<CharOp> entries() {
        return null;
    }
}
