package com.skinny121.mcide.impl.tt;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 24/08/14
 * Time: 10:14 AM
 */
class CharHash<T> extends CharArray<T> {
    private static final int TABLE_SIZE=11;

    @SuppressWarnings("unchecked")
    private T[] array=(T[])new Object[TABLE_SIZE];
    private char[] chars=new char[TABLE_SIZE];
    @Override
    void add(char ch, T t) {
        if(null!=array[ch%TABLE_SIZE]){
           throw new IllegalArgumentException("Invalid hash");
        }
        chars[ch%TABLE_SIZE]=ch;
        array[ch%TABLE_SIZE]=t;
    }

    @Override
    T get(char ch) {
        return chars[ch%TABLE_SIZE]==ch ? array[ch%TABLE_SIZE]:null;
    }

    CharArray<T> merge(CharArray<T> array){
        if(array instanceof CharSingle){
            add(((CharSingle) array).ch, ((CharSingle<T>) array).t);
            if(array.getDefault()!=null){
                if(getDefault()!=null){
                    throw new RuntimeException("Default clash");
                }else{
                    setDefault(array.getDefault());
                }
            }
            return this;
        }
        //array instanceof CharHash<T>
        CharHash<T> hash= ((CharHash<T>) array);
        for(int i=0;i<this.array.length;i++){
            T t=hash.array[i];
            if(t==null)break;
            if(this.array[i]!=null){
                if(this.chars[i]==hash.chars[i]){
                    throw new RuntimeException("Char Clash");
                }else{
                    throw new RuntimeException("Hash Clash");
                }
            }else{
                add(hash.chars[i],t);
            }
        }
        if(array.getDefault()!=null){
            if(getDefault()!=null){
                throw new RuntimeException("Default clash");
            }else{
                setDefault(array.getDefault());
            }
        }
        return this;
    }
}
