package com.skinny121.mcide.impl.tt;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 15/08/14
 * Time: 9:38 AM
 */
class DefaultToTree extends TokenTree {
    private TokenTree tree;
    private TokenTree _default;
    DefaultToTree(TokenTree tree, TokenTree _default) {
        this.tree = tree;
        this._default = _default;
    }

    @Override
    protected CharOp assemble(String name) {
        return new DefaultOp(name);
    }

    private class DefaultOp extends CharOp{
        private CharOp treeA,defaultA;
        private CharArray<CharOp> array;
        private DefaultOp(String name) {
            super(name);
            treeA=tree.assemble(name());
            defaultA=_default.assemble(name());
            array=treeA.entries();
            array.setDefault(defaultA);
        }

        @Override
        public CharArray<CharOp> entries() {
            return array;
        }

        @Override
        public List<CharOp> next() {
            return treeA.next();
        }
    }
}
