package com.skinny121.mcide.impl.tt;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 23/08/14
 * Time: 4:20 PM
 */
abstract class CharArray<T>{
    private T _default;

    abstract void add(char ch,T t);

    void setDefault(T t){
        if(_default!=null)throw new RuntimeException("Default already set");
        this._default=t;
    }

    T getDefault(){
        return _default;
    }

    abstract T get(char ch);

    abstract CharArray<T> merge(CharArray<T> array);
}
