package com.skinny121.mcide.impl.tt;


/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 15/08/14
 * Time: 9:35 AM
 */
class OrTree extends TokenTree {
    private TokenTree left,right;
    OrTree(TokenTree left, TokenTree right) {
        this.left = left;
        this.right = right;
    }

    @Override
    protected CharOp assemble(String name) {
        return new OrOp(name);
    }

    private class OrOp extends CharOp{
        private CharOp leftA,rightA;
        private CharArray<CharOp> array;
        private OrOp(String name){
            super(name);
            leftA=left.assemble(name());
            rightA=right.assemble(name());
            array=leftA.entries().merge(rightA.entries());
        }

        @Override
        public CharArray<CharOp> entries() {
            return array;
        }
    }
}
