package com.skinny121.mcide.impl.tt;

import com.skinny121.mcide.Token;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 24/08/14
 * Time: 9:49 AM
 */
public class CompiledTree {
    private CharOp op;

    CompiledTree(CharOp op){
        this.op=op;
    }

    public Token createToken(String s){
        CharStream stream=new CharStream(s);
        return stream.createToken(processOp(op,stream));
    }

    private String processOp(CharOp op,CharStream stream){
        String t=op.name();
        if(op instanceof ResultOp){
            if(((ResultOp) op).prev)
                stream.back();
        }else if(op instanceof RepeatOp){
            return processOp((RepeatOp)op, stream);
        }else{
            CharOp result;
            if(stream.hasNext()){
                result=op.entries().get(stream.next());
                if(result==null){
                    result=op.entries().getDefault();
                    stream.back();
                }
            }else{
                result=op.entries().getDefault();
            }
            if (result != null) {
                t = processOp(result, stream);
            }
            List<CharOp> nextOps = op.next();
            for (CharOp o : nextOps) {
                t = processOp(o, stream);
            }
        }
        return t;
    }

    private String processOp(RepeatOp op,CharStream stream){
        if(stream.hasNext()){
            CharOp result;
            String t=null;
            result=op.entries().get(stream.next());
            if(result==null){
                result=op.entries().getDefault();
                stream.back();
                return processOp(result,stream);
            }
            t = processOp(result, stream);
            if(stream.hasNext()){
                processOp(op,stream);
            }
            return t;
        }else{
            return processOp(op.entries().getDefault(),stream);
        }

    }
}
