package com.skinny121.mcide.impl.tt;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 15/08/14
 * Time: 9:36 AM
 */
class MayTree extends TokenTree {
    private TokenTree left,right;
    MayTree(TokenTree left, TokenTree right) {
        this.left = left;
        this.right = right;
    }

    @Override
    protected CharOp assemble(String name) {
        return new MayOp(name);
    }

    private class MayOp extends CharOp{
        private CharOp leftA,rightA;
        private List<CharOp> result;
        private MayOp(String name) {
            super(name);
            leftA=left.assemble(name());
            rightA=right.assemble(name());

            result = leftA.next();
            rightA.entries().setDefault(new ResultOp(name(),false));
            result.add(rightA);
            result.addAll(rightA.next());
        }

        @Override
        public CharArray<CharOp> entries() {
            return leftA.entries();
        }

        @Override
        public List<CharOp> next() {
            return result;
        }
    }
}
