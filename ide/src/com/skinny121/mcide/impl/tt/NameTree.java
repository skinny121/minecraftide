package com.skinny121.mcide.impl.tt;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 15/08/14
 * Time: 9:37 AM
 */
class NameTree extends TokenTree {
    private TokenTree tree;
    private String str;
    NameTree(TokenTree tree, String str) {
        this.tree = tree;
        this.str = str;
    }

    @Override
    protected CharOp assemble(String name) {
        return new NameOp();
    }

    private class NameOp extends CharOp {
        private CharOp treeA;
        private NameOp() {
            super(null);
            treeA=tree.assemble(name());
        }

        @Override
        public CharArray<CharOp> entries() {
            return treeA.entries();
        }


        @Override
        public List<CharOp> next() {
            return treeA.next();
        }

        @Override
        public String name() {
            return str;
        }
    }
}
