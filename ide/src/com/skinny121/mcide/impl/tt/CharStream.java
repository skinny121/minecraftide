package com.skinny121.mcide.impl.tt;

import com.skinny121.mcide.Token;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 15/08/14
 * Time: 12:04 PM
 */
class CharStream {
    private String str;
    private int i;

    CharStream(String str){
        this.str=str;
        i=0;
    }

    public boolean hasNext(){
        return i<str.length();
    }

    public char next(){
        return str.charAt(i++);
    }

    public void inc(){
        i++;
    }

    public void back(){
        i--;
    }


    public Token createToken(String tokenType){
        if(tokenType==null)return null;
        return new Token(i,tokenType);
    }
}