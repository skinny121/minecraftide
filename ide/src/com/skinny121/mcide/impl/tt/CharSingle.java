package com.skinny121.mcide.impl.tt;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 24/08/14
 * Time: 10:28 AM
 */
class CharSingle<T> extends CharArray<T>{
    char ch;
    T t;
    CharSingle(char ch,T t){
        this.ch=ch;
        this.t=t;
    }

    @Override
    void add(char ch, T t) {
        throw new RuntimeException("Cannot add to single");
    }

    @Override
    T get(char ch) {
        return this.ch==ch ? t : null;
    }

    @Override
    CharArray<T> merge(CharArray<T> array) {
        if(array instanceof CharHash){
            array.add(ch,t);
            if(array.getDefault()!=null){
                if(getDefault()!=null){
                    throw new RuntimeException("Default clash");
                }
            }else{
                array.setDefault(getDefault());
            }
            return array;
        }else{
            CharHash<T> hash=new CharHash<>();
            hash.add(ch,t);
            hash.add(((CharSingle) array).ch, ((CharSingle<T>) array).t);

            if(array.getDefault()!=null){
                if(getDefault()!=null){
                    throw new RuntimeException("Default clash");
                }else{
                    hash.setDefault(array.getDefault());
                }
            }else{
                hash.setDefault(getDefault());
            }
            return hash;
        }
    }
}
