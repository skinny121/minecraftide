package com.skinny121.mcide.impl.tt;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 15/08/14
 * Time: 9:35 AM
 */
class ThenTree extends TokenTree {
    private TokenTree left,right;
    ThenTree(TokenTree left, TokenTree right) {
        this.left = left;
        this.right = right;
    }

    @Override
    protected CharOp assemble(String name) {
        return new ThenOp(name);
    }

    private class ThenOp extends CharOp{
        private CharOp leftA,rightA;
        private List<CharOp> list;
        private ThenOp(String name) {
            super(name);
            leftA=left.assemble(name());
            rightA=right.assemble(name());

            list = leftA.next();
            rightA.entries().setDefault(new ResultOp("Part:"+name(),false));
            list.add(rightA);
            list.addAll(rightA.next());
        }

        @Override
        public CharArray<CharOp> entries() {
            return leftA.entries();
        }

        @Override
        public List<CharOp> next() {
            return list;
        }
    }
}
