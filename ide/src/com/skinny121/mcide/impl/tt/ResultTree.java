package com.skinny121.mcide.impl.tt;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 25/08/14
 * Time: 9:30 PM
 */
public class ResultTree extends TokenTree {
    private String name;
    private boolean inherit;
    ResultTree(String name){
        this.name=name;
        inherit=false;
    }

    ResultTree(){
        inherit=true;
    }

    @Override
    protected CharOp assemble(String name) {
        return new ResultOp(inherit? name : this.name);
    }
}
