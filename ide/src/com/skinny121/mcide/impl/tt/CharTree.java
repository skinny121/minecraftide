package com.skinny121.mcide.impl.tt;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 15/08/14
 * Time: 9:47 AM
 */
class CharTree extends TokenTree{
    private char ch;
    CharTree(char ch) {
        this.ch = ch;
    }

    @Override
    protected CharOp assemble(String name) {
        return new SingleCharOp(name);
    }

    private class SingleCharOp extends CharOp{
        private CharSingle<CharOp> single;
        private SingleCharOp(String name) {
            super(name);
            single=new CharSingle<>(ch,new ResultOp(name()));
        }

        @Override
        public CharArray<CharOp> entries() {
            return single;
        }
    }
}
