package com.skinny121.mcide.impl.tt;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 14/08/14
 * Time: 9:43 PM
 */
public abstract class TokenTree{
    public TokenTree defaultTo(TokenTree tree){
        return new DefaultToTree(this,tree);
    }

    public TokenTree defaultTo(String name){
        return new DefaultToTree(this,new ResultTree(name));
    }

    public TokenTree or(TokenTree t){
        return new OrTree(this,t);
    }

    public TokenTree then(TokenTree t){
        return new ThenTree(this,t);
    }

    public TokenTree may(TokenTree t){
        return new MayTree(this,t);
    }

    public TokenTree mayRepeat(){
        return new RepeatTree(this);
    }

    public TokenTree name(String name){
        return new NameTree(this,name);
    }

    public CompiledTree build(){
        return new CompiledTree(assemble());
    }

    protected CharOp assemble(){
        return assemble(null);
    }
    protected abstract CharOp assemble(String name);

    public static TokenTree charTree(char c){
        return new CharTree(c);
    }

    public static TokenTree charRange(String name,char lower,char higher){
        if(lower>higher)throw new IllegalStateException("Lower character higher than higher character");
        if(lower==higher)
            return charTree(lower).name(name);
        else
            return charTree(lower).name(name).or(charRange(name, (char) (lower + 1), higher));
    }

    public static TokenTree digit(String name){
        return charRange(name,'0','9');
    }

    public static TokenTree lowerCaseLetter(String name){
        return charRange(name,'a','z');
    }
    public static TokenTree upperCaseLetter(String name){
        return charRange(name,'A','Z');
    }
    public static TokenTree letter(String name){
        return lowerCaseLetter(name).or(upperCaseLetter(name));
    }

    public static TokenTree match(String string){
        if(string.length()==0){
            return new ResultTree();
        }
        TokenTree tree=charTree(string.charAt(0));
        for(int i=1;i<string.length();i++){
            tree=tree.then(charTree(string.charAt(i)));
        }
        return tree;
    }

}
