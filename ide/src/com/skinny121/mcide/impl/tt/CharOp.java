package com.skinny121.mcide.impl.tt;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 23/08/14
 * Time: 4:05 PM
 */
abstract class CharOp {
    private String name;
    CharOp(String name){
        this.name=name;
    }
    abstract CharArray<CharOp> entries();

    List<CharOp> next(){
        return new ArrayList<>();
    }

    String name(){
        return name;
    }
}
