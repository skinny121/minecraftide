package com.skinny121.mcide.impl.tt;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 15/08/14
 * Time: 9:37 AM
 */
class RepeatTree extends TokenTree {
    private TokenTree tree;
    RepeatTree(TokenTree tree) {
        this.tree = tree;
    }

    @Override
    protected CharOp assemble(String name) {
        return new RepeatOp(tree, name);
    }
}
