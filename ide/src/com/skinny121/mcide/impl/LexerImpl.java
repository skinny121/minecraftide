package com.skinny121.mcide.impl;

import com.skinny121.mcide.*;
import com.skinny121.mcide.Node;
import java.util.*;
import java.util.function.*;


/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 30/03/14
 * Time: 3:20 PM
 */
public abstract class LexerImpl extends AbstractLexer {
    private TokenList tokenList;
    private String text;
    protected TokenContext context;
    private UnaryOperator<Node> endIterator=n->n.getPrev()==null?tokenList.getFirstNode():n.getPrev().getNext();
    private UnaryOperator<Node> startIterator=Node::getPrev;

    public LexerImpl(List<Token> tokens, String text){
        this.text=text;
        this.tokenList=new TokenList(tokens);
        context=createTokenContext();
        checkFormat();
    }

    public abstract List<Token> tokenise(String text,int offset);

    @Override
    public TokenContext createTokenContext() {
        return new TokenContext(tokenList,()->this.text);
    }

    @Override
    public void insertText(String text, int offset) {
        if(tokenList.isEmpty()){
            //special case where existing text is empty
            if(offset!=0)throw new IllegalStateException("offset is non-zero but has no tokens");
            if(!this.text.isEmpty())throw new IllegalStateException("text is non-empty but has no tokens");
            this.text=text;
            tokenList.addAllFirst(tokenise(text,0));
        }else if(offset==this.text.length()){
            this.text+=text;
            tokenList.addAllLast(tokenise(text,offset));
            mergeIter(tokenList.getLastNode().getPrev(), startIterator);

        }else{
            this.text=this.text.substring(0,offset)+text+this.text.substring(offset);

            Node node=context.getNodeOfStringIndex(offset);
            Node endNode;
            List<Token> insertedTokens=tokenise(text,offset);
            int offsetInToken=offset-node.getStartStringIndex();
            if(offsetInToken!=0){
                //break token in two
                Token[] splitToken=split(node,offsetInToken);
                this.tokenList.replace(node, splitToken[0]);
                endNode=this.tokenList.addAfter(node, splitToken[1]);
                this.tokenList.addAllAfter(node, insertedTokens);
            }else{
                node=node.getPrev();
                endNode=node;
                this.tokenList.addAllBefore(node, insertedTokens);
            }

            //merge towards end
            mergeIter(endNode.getNext(),endIterator);
            //merge towards start
            mergeIter(node, startIterator);
        }

        //notify listeners
        notifyInsert(new LexerEventInsert(null,null,null));
        //check format
        checkFormat();
    }

    private void mergeIter(Node node,UnaryOperator<Node> updator){
        Token[] tmp;

        while(node!=null && node.getNext()!=null){
            tmp=merge(node,node.getNext());
            if(tmp==null)break;
            tokenList.remove(node);
            tokenList.remove(node.getNext());
            tokenList.addAllAfter(node.getPrev(),Arrays.asList(tmp));

            node=updator.apply(node);
        }
    }

    @Override
    public void removeText(int offset, int length) {
        if(text.isEmpty() || length==0)return;
        this.text=this.text.substring(0,offset)+this.text.substring(offset+length);

        Node start=context.getNodeOfStringIndex(offset);
        Node end=context.getNodeOfStringIndex(offset + length -1);

        int offsetInToken;

        //remove applicable tokens
        if(start!=end){
            for(Node curr=start.getNext();curr!=end;curr=curr.getNext()){
                tokenList.remove(curr);
            }
            offsetInToken=end.getEndStringIndex()-offset-length;
            //remove end
            if(offsetInToken==0){
                tokenList.remove(end);
            }else{
                tokenList.resize(end,offsetInToken);
            }
        }

        offsetInToken=offset-start.getStartStringIndex();
        if(offsetInToken==0){
            tokenList.remove(start);
            start=start.getPrev();
        }else{
            tokenList.resize(start,offsetInToken);
        }

        if(tokenList.size()!=1){
            //merge towards end
            mergeIter(start,endIterator);
            //merge towards start
            if(start!=null)mergeIter(start.getPrev(),startIterator);
        }else{
            tokenList.remove(start);
            tokenList.addAllFirst(tokenise(this.text,-1));
        }

        //notify listeners
        notifyDelete(new LexerEventDelete(null,null));
        //check format
        checkFormat();
    }

    //debug method to check format of the token list
    private boolean checkFormat(){
        return context.checkFormat(text.length());
    }

    public abstract Token[] split(Node t,int offset);
    public abstract Token[] merge(Node t1,Node t2);

    @Override
    public String toString() {
        return tokenList.toString()+" "+text;
    }
}