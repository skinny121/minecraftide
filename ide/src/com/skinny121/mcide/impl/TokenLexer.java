package com.skinny121.mcide.impl;

import com.skinny121.mcide.Node;
import com.skinny121.mcide.Token;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 10/07/14
 * Time: 6:24 PM
 */
public class TokenLexer extends LexerImpl {

    private TokenRegister register;
    public TokenLexer(TokenRegister register, String text) {
        super(tokeniseStatic(register, text,0), text);
        this.register=register;
    }

    private static List<Token> tokeniseStatic(TokenRegister register,String text,int offset){
        List<Token> result=new ArrayList<>();
        List<TokenType> types=register.getTokenTypes();
        int endOffset=offset+text.length();
        token:
        while(offset<endOffset){
            for(TokenType type:types){
                int len=type.createToken(text);
                if(len==-1)continue;
                result.add(new Token(len,type.getTokenName()));
                offset+=len;
                text=text.substring(len);
                continue token;
            }
            throw new IllegalStateException("No type matches "+text);
        }
        return result;
    }

    @Override
    public List<Token> tokenise(String text,int offset){
        return tokeniseStatic(register,text,offset);
    }
    @Override
    public Token[] split(Node t, int offset) {
        String s=context.getString(t);
        List<Token> tokens=tokenise(s.substring(0,offset),t.getStartStringIndex());
        tokens.addAll(tokenise(s.substring(offset),t.getStartStringIndex()+offset));
        return tokens.stream().toArray(Token[]::new);
    }

    @Override
    public Token[] merge(Node t1, Node t2) {
        String text=context.getString(t1)+context.getString(t2);
        return tokenise(text,t1.getStartStringIndex()).stream().toArray(Token[]::new);
    }
}
