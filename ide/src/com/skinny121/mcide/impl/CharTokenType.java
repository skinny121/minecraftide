package com.skinny121.mcide.impl;


import java.util.Collections;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 12/07/14
 * Time: 9:15 PM
 */
public class CharTokenType extends AbstractTokenType {
    private char ch;
    public CharTokenType(char ch, List<TokenType> dependencies) {
        super(Character.toString(ch), dependencies);
        this.ch=ch;
    }

    public CharTokenType(char ch) {
        this(ch, Collections.emptyList());
    }

    @Override
    public int createToken(String s) {
        return s.charAt(0)==ch ? 1:-1;
    }
}
