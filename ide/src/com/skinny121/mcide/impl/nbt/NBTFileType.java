package com.skinny121.mcide.impl.nbt;

import com.skinny121.mcide.*;
import com.skinny121.mcide.TokenContext.Builder;
import com.skinny121.mcide.impl.*;
import com.skinny121.mcide.impl.nbt.tree.*;
import com.skinny121.nbt.Exporter;
import com.skinny121.nbt.Importer;
import com.skinny121.nbt.JsonToNBT;
import com.skinny121.nbt.NBTToJson;
import com.skinny121.mcide.tree.ASTNode;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;

//import static com.skinny121.mcide.impl.nbt.NBTLexer.*;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 30/03/14
 * Time: 2:51 PM
 */
public class NBTFileType implements FileType{
    //matches files with extension .dat/.mcr/.mca/.dat_mcr/.dat_mcr/.dat_old
    private static final Predicate<String> pattern=Pattern.compile("^.*\\.(dat|mcr|mca|dat_mcr|dat_old)$").asPredicate();
    private static NBTFileType fileType;

    private static TokenRegister tokenRegister=new TokenRegister();
    public static final TokenType
            LEFT_BRACE,RIGHT_BRACE,
            LEFT_BRACKET,RIGHT_BRACKET,
            COMMA,
            COLON,
            QUOTATION,
            SLASH,
            BOOLEAN,
            NULL,
            SIGN,
            SPACE,
            STRING,
            BYTE,
            SHORT,
            INT,
            LONG,
            FLOAT,
            DOUBLE;
    static{
        LEFT_BRACE=tokenRegister.register(new CharTokenType('{'));
        RIGHT_BRACE=tokenRegister.register(new CharTokenType('}'));
        LEFT_BRACKET=tokenRegister.register(new CharTokenType('['));
        RIGHT_BRACKET=tokenRegister.register(new CharTokenType(']'));
        COMMA=tokenRegister.register(new CharTokenType(','));
        COLON=tokenRegister.register(new CharTokenType(':'));
        QUOTATION=tokenRegister.register(new CharTokenType('"'));
        tokenRegister.register(new CharTokenType('.'));
        SLASH=tokenRegister.register(new CharTokenType('\\'));
        BOOLEAN=tokenRegister.register(new RegexTokenType("boolean","true|false"));
        NULL=tokenRegister.register(new RegexTokenType("null","null"));
        SIGN=tokenRegister.register(new RegexTokenType("sign","\\-|\\+"));
        BYTE=tokenRegister.register(new RegexTokenType("byte","(\\-|\\+)?[0-9]+b"));
        SHORT=tokenRegister.register(new RegexTokenType("short","(\\-|\\+)?[0-9]+s"));
        INT=tokenRegister.register(new RegexTokenType("int","(\\-|\\+)?[0-9]+"));
        LONG=tokenRegister.register(new RegexTokenType("long","(\\-|\\+)?[0-9]+l"));
        FLOAT=tokenRegister.register(new RegexTokenType("float","(\\-|\\+)?[0-9]+\\.[0-9]+f"));
        DOUBLE=tokenRegister.register(new RegexTokenType("double","(\\-|\\+)?[0-9]+\\.[0-9]+d?"));

        tokenRegister.register(new RegexTokenType("number:part-num.","(\\-|\\+)?[0-9]+\\."));
        tokenRegister.register(new RegexTokenType("number:part-.num","\\.[0-9]+"));

        SPACE=tokenRegister.register(new RegexTokenType("space","[\n\r\u0009\u0020]+"));
        STRING=tokenRegister.registerLast(new RegexTokenType("string","[^{}\\[\\],:\\.\\\\0-9\\-\\+\n\r\u0009\u0020]+"));
    }

    public static NBTFileType getInstance(){
        if (fileType == null) {
            fileType = new NBTFileType();
        }
        return fileType;
    }
    private ErrorHandler errorHandler=new DefaultErrorHandler();
    @Override
    public void setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler=errorHandler;
    }

    @Override
    public boolean accept(String name) {
        return pattern.test(name);
    }

    @Override
    public Lexer lex(String text) {
        return new TokenLexer(tokenRegister,text);
    }

    @Override
    public ASTNode parse(Lexer tokens) {
        return createAST(tokens);
    }

    private FileNode createAST(Lexer lexer){
        TokenContext tokens=lexer.createTokenContext();
        //filter out spaces
        tokens.filter(t->!SPACE.isOf(t));
        return new FileNode(tokens.subContextAll(), createKeyValue(tokens));
    }

    private KeyValueNode createKeyValue(TokenContext tokens){
        NameNode name;
        Builder builder=tokens.builder();
        if(STRING.isOf(tokens.token())){
            name=new NameNode(tokens.subContext());
        }else{
            //empty name
            name=new NameNode(null);
        }
        if(!COLON.isOf(tokens.nextToken())){
            errorHandler.handleError(ErrorLevel.ERROR,"expected :",tokens.node());
        }
        //move to start of value
        tokens.nextToken();
        ASTNode value=createValue(tokens);
        return new KeyValueNode(builder.build(),name,value);
    }

    private void compoundCheck(Node node){
        if(!RIGHT_BRACE.isOf(node.getToken())){
            errorHandler.handleError(ErrorLevel.ERROR,"expected }",node);
        }
    }

    private CompoundNode createCompound(TokenContext tokens){
        return createCollection(tokens,this::createKeyValue,this::compoundCheck,CompoundNode::new);
    }

    private void listCheck(Node node){
        if(!RIGHT_BRACKET.isOf(node.getToken())){
            errorHandler.handleError(ErrorLevel.ERROR,"expected ]",node);
        }
    }

    private ListNode createList(TokenContext tokens){
        return createCollection(tokens,this::createValue,this::listCheck,ListNode::new);
    }

    private <T,R> R createCollection(TokenContext tokens,
                                            Function<TokenContext,T> function,
                                            Consumer<Node> check,
                                            CollectionGen<T,R> gen){
        ArrayList<T> list=new ArrayList<>();
        Builder builder=tokens.builder();
        do{
            tokens.nextToken();
            list.add(function.apply(tokens));
        }while(COMMA.isOf(tokens.token()));
        check.accept(tokens.node());
        tokens.nextToken();
        return gen.create(builder.build(), list);
    }

    private ASTNode createValue(TokenContext tokens){
        Node start=tokens.node();
        TokenContext primContext=tokens.subContext();
        if(BOOLEAN.isOf(start.getToken())){
            tokens.nextToken();
            return new BooleanNode(primContext);
        }else if(BYTE.isOf(start.getToken())){
            tokens.nextToken();
            return new ByteNode(primContext);
        }else if(SHORT.isOf(start.getToken())){
            tokens.nextToken();
            return new ShortNode(primContext);
        }else if(INT.isOf(start.getToken())){
            tokens.nextToken();
            return new IntNode(primContext);
        }else if(LONG.isOf(start.getToken())){
            tokens.nextToken();
            return new LongNode(primContext);
        }else if(FLOAT.isOf(start.getToken())){
            tokens.nextToken();
            return new FloatNode(primContext);
        }else if(DOUBLE.isOf(start.getToken())){
            tokens.nextToken();
            return new DoubleNode(primContext);
        }else if(NULL.isOf(start.getToken())){
            tokens.nextToken();
            return new NullNode(primContext);
        }else if(STRING.isOf(start.getToken())){
            tokens.nextToken();
            return new StringNode(primContext);
        }else if(QUOTATION.isOf(start.getToken())){
            return parseString(tokens);
        }else if(LEFT_BRACE.isOf(start.getToken())){
            return createCompound(tokens);
        }else if(LEFT_BRACKET.isOf(start.getToken())){
            return createList(tokens);
        }else{
            errorHandler.handleError(ErrorLevel.ERROR,"invalid value",start);
            return null;
        }
    }

    private StringNode parseString(TokenContext tokens){
        Builder builder=tokens.builder();
        tokens.nextToken();
        boolean escaped=false;
        while(!QUOTATION.isOf(tokens.nextToken()) || escaped){
            escaped = !escaped && SLASH.isOf(tokens.token());
        }
        tokens.nextToken();
        return new StringNode(builder.build());
    }

    @Override
    public String decode(InputStream in) throws IOException {
        return NBTToJson.toJson(
                Importer.importNBT(in),
                new NBTToJson.Options(true));
    }

    @Override
    public void encode(String text, OutputStream out) throws IOException {
        Exporter.export(out,JsonToNBT.toNBT(text));
    }

}
