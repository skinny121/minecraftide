package com.skinny121.mcide.impl.nbt.tree;

import com.skinny121.mcide.TokenContext;
import com.skinny121.mcide.tree.ASTNode;

import java.util.stream.Stream;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 1/04/14
 * Time: 12:23 PM
 */
public class KeyValueNode extends ASTNode {
    private NameNode name;
    private ASTNode value;
    public KeyValueNode(TokenContext tokenContext, NameNode name,ASTNode value) {
        super(tokenContext);
        this.name=name;
        this.value=value;
    }

    @Override
    public Stream<? extends ASTNode> getChildren() {
        return Stream.of(name,value);
    }
}
