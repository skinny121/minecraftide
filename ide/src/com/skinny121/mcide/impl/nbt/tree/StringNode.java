package com.skinny121.mcide.impl.nbt.tree;

import com.skinny121.mcide.Node;
import com.skinny121.mcide.Token;
import com.skinny121.mcide.TokenContext;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 1/04/14
 * Time: 12:59 PM
 */
public class StringNode extends PrimitiveNode<String> {

    public StringNode(TokenContext tokenContext) {
        super(tokenContext);
    }

    @Override
    protected String initValue(Node n) {
        String str=tokenContext.getString(n);
        String result="";
        boolean escaped=false;
        for(int i=1;i<str.length()-1;i++){
            char ch=str.charAt(i);
            if(ch!='"' || escaped)result+=ch;
            escaped=ch=='\\';
        }
        return result;
    }
}
