package com.skinny121.mcide.impl.nbt.tree;

import com.skinny121.mcide.Node;
import com.skinny121.mcide.TokenContext;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 1/04/14
 * Time: 12:41 PM
 */
public class NameNode extends PrimitiveNode<String> {

    public NameNode(TokenContext tokenContext) {
        super(tokenContext);
    }

    @Override
    protected String initValue(Node n) {
        return tokenContext.size()==0 ? "" : tokenContext.getString(n);
    }


}
