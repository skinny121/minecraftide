package com.skinny121.mcide.impl.nbt.tree;

import com.skinny121.mcide.Node;
import com.skinny121.mcide.Token;
import com.skinny121.mcide.TokenContext;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 1/04/14
 * Time: 1:19 PM
 */
public class IntNode extends PrimitiveNode<Integer> {

    public IntNode(TokenContext tokenContext) {
        super(tokenContext);
    }

    @Override
    protected Integer initValue(Node n) {
        return Integer.parseInt(tokenContext.getString(n));
    }
}
