package com.skinny121.mcide.impl.nbt.tree;

import com.skinny121.mcide.Token;
import com.skinny121.mcide.TokenContext;
import com.skinny121.mcide.tree.ASTNode;

import java.util.stream.Stream;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 2/04/14
 * Time: 10:32 PM
 */
public class NullNode extends ASTNode {

    public NullNode(TokenContext tokenContext) {
        super(tokenContext);
    }

    @Override
    public Stream<? extends ASTNode> getChildren() {
        return Stream.empty();
    }
}
