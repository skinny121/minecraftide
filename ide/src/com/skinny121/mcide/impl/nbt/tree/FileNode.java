package com.skinny121.mcide.impl.nbt.tree;

import com.skinny121.mcide.Lexer;
import com.skinny121.mcide.Token;
import com.skinny121.mcide.TokenContext;
import com.skinny121.mcide.tree.ASTNode;

import java.util.stream.Stream;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 31/03/14
 * Time: 1:14 PM
 */
public class FileNode extends ASTNode {
    private KeyValueNode rootNode;

    public FileNode(TokenContext tokenContext,KeyValueNode rootNode) {
        super(tokenContext);
        this.rootNode=rootNode;
    }

    @Override
    public Stream<KeyValueNode> getChildren() {
        return Stream.of(rootNode);
    }
}
