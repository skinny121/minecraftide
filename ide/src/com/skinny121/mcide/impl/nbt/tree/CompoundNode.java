package com.skinny121.mcide.impl.nbt.tree;

import com.skinny121.mcide.TokenContext;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 1/04/14
 * Time: 12:48 PM
 */
public class CompoundNode extends CollectionNode<KeyValueNode> {
    public CompoundNode(TokenContext tokenContext, List<KeyValueNode> children) {
        super(tokenContext, children);
    }
}
