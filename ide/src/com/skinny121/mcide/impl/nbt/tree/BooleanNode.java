package com.skinny121.mcide.impl.nbt.tree;

import com.skinny121.mcide.Node;
import com.skinny121.mcide.Token;
import com.skinny121.mcide.TokenContext;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 2/04/14
 * Time: 10:31 PM
 */
public class BooleanNode extends PrimitiveNode<Boolean> {
    public BooleanNode(TokenContext tokenContext) {
        super(tokenContext);
    }

    @Override
    protected Boolean initValue(Node n) {
        return Boolean.getBoolean(tokenContext.getString(n));
    }
}
