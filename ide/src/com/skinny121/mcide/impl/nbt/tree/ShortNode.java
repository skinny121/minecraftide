package com.skinny121.mcide.impl.nbt.tree;

import com.skinny121.mcide.Node;
import com.skinny121.mcide.Token;
import com.skinny121.mcide.TokenContext;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 1/04/14
 * Time: 1:18 PM
 */
public class ShortNode extends PrimitiveNode<Short> {
    public ShortNode(TokenContext tokenContext) {
        super(tokenContext);
    }

    @Override
    protected Short initValue(Node n) {
        String str=tokenContext.getString(n);
        return Short.parseShort(str.substring(0, str.length() - 1));
    }
}
