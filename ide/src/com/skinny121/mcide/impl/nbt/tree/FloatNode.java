package com.skinny121.mcide.impl.nbt.tree;

import com.skinny121.mcide.Node;
import com.skinny121.mcide.Token;
import com.skinny121.mcide.TokenContext;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 1/04/14
 * Time: 1:21 PM
 */
public class FloatNode extends PrimitiveNode<Float> {
    public FloatNode(TokenContext tokenContext) {
        super(tokenContext);
    }

    @Override
    protected Float initValue(Node n) {
        String str=tokenContext.getString(n);
        return Float.parseFloat(str.substring(0, str.length() - 1));
    }
}
