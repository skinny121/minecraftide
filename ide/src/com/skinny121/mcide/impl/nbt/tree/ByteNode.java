package com.skinny121.mcide.impl.nbt.tree;

import com.skinny121.mcide.Node;
import com.skinny121.mcide.Token;
import com.skinny121.mcide.TokenContext;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 1/04/14
 * Time: 1:16 PM
 */
public class ByteNode extends PrimitiveNode<Byte> {

    public ByteNode(TokenContext tokenContext) {
        super(tokenContext);
    }

    @Override
    protected Byte initValue(Node n) {
        String str=tokenContext.getString(n);
        return Byte.parseByte(str.substring(0,str.length()-1));
    }
}
