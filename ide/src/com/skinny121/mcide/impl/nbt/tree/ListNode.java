package com.skinny121.mcide.impl.nbt.tree;

import com.skinny121.mcide.TokenContext;
import com.skinny121.mcide.tree.ASTNode;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 1/04/14
 * Time: 12:23 PM
 */
public class ListNode extends CollectionNode<ASTNode> {

    public ListNode(TokenContext tokenContext, List<ASTNode> children) {
        super(tokenContext, children);
    }
}
