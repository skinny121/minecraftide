package com.skinny121.mcide.impl.nbt.tree;

import com.skinny121.mcide.TokenContext;
import com.skinny121.mcide.tree.ASTNode;

import java.util.List;
import java.util.stream.Stream;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 31/03/14
 * Time: 1:36 PM
 */
public class CollectionNode<T extends ASTNode> extends ASTNode{
    private List<T> children;
    protected CollectionNode(TokenContext tokenContext,List<T> children) {
        super(tokenContext);
        this.children=children;
    }

    @Override
    public Stream<T> getChildren() {
        return children.stream();
    }
}
