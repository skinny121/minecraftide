package com.skinny121.mcide.impl.nbt.tree;

import com.skinny121.mcide.Node;
import com.skinny121.mcide.Token;
import com.skinny121.mcide.TokenContext;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 1/04/14
 * Time: 1:22 PM
 */
public class DoubleNode extends PrimitiveNode<Double> {

    public DoubleNode(TokenContext tokenContext) {
        super(tokenContext);
    }

    @Override
    protected Double initValue(Node n) {
        String str=tokenContext.getString(n);
        return Double.parseDouble(str.matches(".*d") ? str.substring(0,str.length()-1) : str);
    }
}
