package com.skinny121.mcide.impl.nbt.tree;

import com.skinny121.mcide.Node;
import com.skinny121.mcide.Token;
import com.skinny121.mcide.TokenContext;
import com.skinny121.mcide.tree.ASTNode;

import java.util.stream.Stream;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 1/04/14
 * Time: 12:24 PM
 */
public abstract class PrimitiveNode<T> extends ASTNode{
    private T value;

    protected PrimitiveNode(TokenContext tokenContext) {
        super(tokenContext);
        value=initValue(tokenContext.startNode());
    }

    protected abstract T initValue(Node n);

    public T getValue(){
        return value;
    }

    @Override
    public Stream<ASTNode> getChildren() {
        return Stream.empty();
    }
}
