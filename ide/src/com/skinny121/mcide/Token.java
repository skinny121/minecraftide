package com.skinny121.mcide;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 30/03/14
 * Time: 12:22 PM
 */
public class Token {
    private int length;
    private String tokenType;

    public Token(int length, String tokenType) {
        this.length = length;
        this.tokenType = tokenType;
    }

    public int getLength(){
        return length;
    }

    public String getTokenType(){
        return tokenType;
    }

    void resize(int newsize){
        this.length=newsize;
    }

    @Override
    public String toString() {
        return tokenType+"("+length+")";
    }
}
