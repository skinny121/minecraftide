package com.skinny121.mcide;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 6/07/14
 * Time: 4:10 PM
 */
public interface ErrorHandler {

    void handleError(ErrorLevel level,String message);

    void handleError(ErrorLevel level,String message,int start,int end);

    default void handleError(ErrorLevel level,String message,TokenContext context){
        if(context.size()>0){
            handleError(level, message,
                    context.startNode().getStartStringIndex(),
                    context.endNode().getEndStringIndex());
        }
    }

    default void handleError(ErrorLevel level,String message,Node node){
        handleError(level, message,
                    node.getStartStringIndex(),
                    node.getEndStringIndex());
    }
}
