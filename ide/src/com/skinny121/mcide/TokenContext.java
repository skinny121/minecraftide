package com.skinny121.mcide;

import java.util.function.*;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 31/03/14
 * Time: 1:16 PM
 */
public class TokenContext {
    private TokenList list;
    //reference to the text
    private Supplier<String> supplier;
    private Predicate<Token> accept=t->true;
    private Node curr;
    private TokenContext parent;
    private TokenContext(TokenList list,Supplier<String> supplier,TokenContext parent){
        this.list=list;
        this.supplier = supplier;
        this.parent=parent;
    }
    public TokenContext(TokenList tokens,Supplier<String> supplier){
        this(tokens,supplier,null);
    }
    public Token getTokenAtStringIndex(int index){
        return checkNull(getNodeOfStringIndex(index));
    }

    public Node getNodeOfStringIndex(int index){
        return list.search(t->index<t.getEndStringIndex());
    }

    private String getString(int start,int end){
        return supplier.get().substring(start,end);
    }
    public String getString(){
        return getString(startNode().getStartStringIndex(), endNode().getEndStringIndex());
    }

    public String getString(Node node){
        return getString(node.getStartStringIndex(),node.getEndStringIndex());
    }

    public int size(){
        return list.size();
    }

    public int length() {
        return endNode().getEndStringIndex() - startNode().getStartStringIndex();
    }

    public Token token(){
        return checkNull(curr);
    }

    public Node node(){
        return curr;
    }

    public Token nextToken(){
        return checkNull(nextNode());
    }

    public Node nextNode(){
        return moveNode(1);
    }

    public Token move(int i){
        return checkNull(moveNode(i));
    }

    public Node moveNode(int i){
        curr=peekNode(i);
        return curr;
    }

    public Token peek(){
        return peek(1);
    }

    public Token peek(int i){
        return checkNull(peekNode(i));
    }

    public Node peekNode(int i){
        if(i>0){
            return iterate(curr,i,Node::getNext);
        }else{
            return iterate(curr,-i,Node::getPrev);
        }
    }

    private Node iterate(Node n,int i,Function<Node,Node> function){
        while(n!=null){
            if(i==0)return n;
            n=function.apply(n);
            if(accept.test(n.getToken()))
                i--;
        }
        return null;
    }

    public Token startToken(){
        return list.getFirst();
    }

    public Node startNode() { return list.getFirstNode();}

    public Token endToken(){
        return list.getLast();
    }

    public Node endNode() { return list.getLastNode();}

    public boolean hasNext(){
        while(curr!=null){
            if(accept.test(curr.token))
                return true;
            curr=curr.getNext();
        }
        return false;
    }

    private Token checkNull(Node n){
        return n!=null ? n.getToken() : null;
    }

    public TokenContext subContext(Node start,Node end){
        TokenContext context=new TokenContext(list.subList(start,end),supplier,this);
        context.accept=accept;
        return context;
    }

    public TokenContext subContext(Node node){
        return subContext(node,node);
    }

    public TokenContext subContext(){
        return subContext(curr);
    }

    public TokenContext subContextAll(){
        return subContext(list.getFirstNode(),list.getLastNode());
    }

    public Builder builder(){
        return builder(curr);
    }

    public Builder builder(Node start){
        return new Builder(start);
    }

    public void filter(Predicate<Token> accept){
        this.accept=accept;
    }

    public boolean checkFormat(int expectedLength){
        return list.checkFormat(expectedLength);
    }

    @Override
    public String toString() {
        String str="Current Node:";
        str+=curr;
        str+="\n";
        str+=list.toString();
        return str;
    }

    public class Builder{
        private Node start;
        private Builder(Node start){
            this.start=start;
        }

        public TokenContext build(){
            return build(curr);
        }

        public TokenContext build(Node node){
            return subContext(start,node);
        }
    }
}
