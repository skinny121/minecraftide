package com.skinny121.mcide;

/**
* Created with IntelliJ IDEA.
* User: BenLewis
* Date: 8/08/14
* Time: 5:46 PM
*/
public class Node {
    protected final Token token;
    protected Node prev;
    protected Node next;
    protected int strIndex;

    Node(Token token, Node prev, Node next){
        this.token=token;
        this.prev=prev;
        this.next=next;
    }

    public Token getToken() {
        return token;
    }

    public Node getPrev() {
        return prev;
    }

    public Node getNext() {
        return next;
    }

    public int getStartStringIndex(){
        return strIndex;
    }

    public int getEndStringIndex(){
        return strIndex+token.getLength();
    }
}
