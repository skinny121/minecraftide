package com.skinny121.mcide;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 30/03/14
 * Time: 12:54 PM
 */
public class LexerEventInsert {
    private List<Token> tokens;
    private LexerEventType before;
    private LexerEventType after;

    public LexerEventInsert(List<Token> tokens, LexerEventType before, LexerEventType after) {
        this.tokens = tokens;
        this.before = before;
        this.after = after;
    }

    public List<Token> getTokens() {
        return tokens;
    }

    public LexerEventType getBeforeEventType() {
        return before;
    }

    public LexerEventType getAfterEventType() {
        return after;
    }
}
