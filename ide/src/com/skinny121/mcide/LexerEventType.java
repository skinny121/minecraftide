package com.skinny121.mcide;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 30/03/14
 * Time: 1:09 PM
 */
public enum LexerEventType {
    NOT_CHANGED,
    CHANGED,
    EXTENDED,
    SHRUNK,
    MERGED
}
