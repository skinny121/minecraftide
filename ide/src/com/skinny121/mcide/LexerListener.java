package com.skinny121.mcide;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 30/03/14
 * Time: 12:52 PM
 */
@FunctionalInterface
public interface LexerListener<T> {
    void lexerChanged(T t);
}
