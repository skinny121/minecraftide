package com.skinny121.mcide;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 30/03/14
 * Time: 12:18 PM
 */
public interface Lexer {

    TokenContext createTokenContext();

    void insertText(String text,int offset);

    void removeText(int offset,int length);

    void addLexerInsertListener(LexerListener<LexerEventInsert> listener);
    void removeLexerInsertListener(LexerListener<LexerEventInsert> listener);

    void addLexerDeleteListener(LexerListener<LexerEventDelete> listener);
    void removeLexerDeleteListener(LexerListener<LexerEventDelete> listener);
}
