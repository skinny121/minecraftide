package com.skinny121.mcide;

import java.util.List;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 14/07/14
 * Time: 9:58 PM
 */
public class TokenList{
    private Node first;
    private Node last;
    private TokenList parent;
    public TokenList(){
        first=null;
        last=null;
    }

    public TokenList(List<Token> tokens){
        this();
        addAllFirst(tokens);
    }

    private TokenList(Node first,Node last,TokenList parent){
        this.first=first;
        this.last=last;
        this.parent=parent;
    }

    public Node addAfter(Node index,Token token){
        return calcFormat(addAfter(()->index,token));
    }

    private Node addAfter(Supplier<Node> indexSupplier, Token token){
        if(isEmpty()){
            return addIfEmpty(token);
        }else {
            Node index=indexSupplier.get();
            if(index==null){
                return addFirst(token);
            }else if(index==last){
                return addLast(token);
            }else{
                Node n=new Node(token,index,index.next);
                index.next.prev=n;
                index.next=n;
                return n;
            }
        }
    }

    public Node addAllAfter(Node index,List<Token> tokens){
        if(tokens.isEmpty())return null;
        return addAllAfter(() -> index, tokens);
    }
    private Node addAllAfter(Supplier<Node> indexSupplier,List<Token> tokens){
        if(isEmpty()){
            return addAllFirst(tokens);
        }else{
            Node node=indexSupplier.get();
            if(node==null){
                return addAllFirst(tokens);
            }else{
                for(int i=tokens.size()-1;i>=1;i--){
                    addAfter(()->node,tokens.get(i));
                }
                return addAfter(node,tokens.get(0));
            }
        }
    }
    public Node addBefore(Node index,Token token){
        return calcFormat(addBefore(()->index,token));
    }

    private Node addBefore(Supplier<Node> indexSupplier, Token token){
        if(isEmpty()){
            return addIfEmpty(token);
        }else{
            Node index=indexSupplier.get();
            if(index==null){
                return addLast(token);
            }else if(index==first){
                return addFirst(token);
            }else{
                Node n=new Node(token,index.prev,index);
                index.prev=n;
                index.prev.next=n;
                return n;
            }
        }
    }
    public Node addAllBefore(Node index,List<Token> tokens){
        if(tokens.isEmpty())return null;
        return addAllBefore(()->index,tokens);
    }

    private Node addAllBefore(Supplier<Node> index,List<Token> tokens){
        if(isEmpty()){
            return addAllLast(tokens);
        }else{
            Node node=index.get();
            if(node==null){
                return addAllLast(tokens);
            }else{
                Node n=addBefore(()->node,tokens.get(0));
                for(int i=1;i<tokens.size();i++){
                    addBefore(()->node,tokens.get(i));
                }
                return calcFormat(n);
            }
        }
    }

    public Node addFirst(Token token){
        return calcFormat(addFirstI(token));
    }

    private Node addFirstI(Token token){
        if(isEmpty()){
            return addIfEmpty(token);
        }else{
            Node node=new Node(token,first.prev,first);
            if(first.prev!=null)first.prev.next=node;
            first.prev=node;
            setFirst(node);
            return node;
        }

    }

    public Node addAllFirst(List<Token> tokens){
        if(tokens.isEmpty())return null;
        for(int i=tokens.size()-1;i>=1;i--){
            addFirstI(tokens.get(i));
        }
        return addFirst(tokens.get(0));
    }

    private void setFirst(Node node){
        if(parent!=null && parent.first==first)parent.setFirst(node);
        first=node;
    }

    public Node addLast(Token token){
        return calcFormat(addLastI(token));
    }

    private Node addLastI(Token token){
        if(isEmpty()){
            return addIfEmpty(token);
        }else{
            Node node=new Node(token,last,last.next);
            if(last.next!=null)last.next.prev=node;
            last.next=node;
            setLast(node);
            return node;
        }
    }

    public Node addAllLast(List<Token> tokens){
        if(tokens.isEmpty())return null;
        Node n=addLastI(tokens.get(0));
        for(int i=1;i<tokens.size();i++){
            addLastI(tokens.get(i));
        }
        return calcFormat(n);
    }

    private void setLast(Node node){
        if(parent!=null && parent.first==first)parent.setLast(node);
        last=node;
    }

    private Node addIfEmpty(Token token){
        Node node=new Node(token,null,null);
        setFirst(node);
        setLast(node);
        return node;
    }

    public Node replace(Node old,Token _new){
        Node node=new Node(_new,old.prev,old.next);
        if(node.getNext()!=null)node.getNext().prev=node;
        node.getPrev().next=node;
        return calcFormat(node);
    }

    public Node resize(Node node,int newsize){
        node.getToken().resize(newsize);
        return calcFormat(node);
    }

    public void remove(Node node){
        if(node==null)return;
        if(node==first){
            first=first.next;
        }else{
            node.prev.next=node.next;
        }

        if(node==last){
            last=last.prev;
        }else{
            node.next.prev=node.prev;
        }
        calcFormat(node.getPrev()!=null?node.getPrev():first);
    }

    public boolean isEmpty(){
        return first==null;
    }

    public Node getNode(Token token){
        if(last.token.equals(token))return last;
        for(Node curr=first;curr!=last;curr=curr.next){
            if(curr.token.equals(token)){
                return curr;
            }
        }
        return null;
    }

    public Node getNode(int index){
        Node n=first;
        for(;index>0;index--){
            n=n.next;
            if(n==null)return null;
        }
        return n;
    }

    public Token get(int index){
        return getNode(index).token;
    }

    public Token getFirst(){
        return first.token;
    }

    public Node getFirstNode(){
        return first;
    }

    public Token getLast(){
        return last.token;
    }

    public Node getLastNode(){
        return last;
    }


    public int size(){
        if(isEmpty())return 0;
        int size=1;
        for(Node n=first;n!=last;n=n.next){
            size++;
        }
        return size;
    }

    public Node search(Predicate<Node> predicate){
        for(Node curr=first;curr!=null;curr=curr.next){
            if(predicate.test(curr)){
                return curr;
            }
        }
        return null;
    }

    //debug method to check format of the token list
    public boolean checkFormat(int expectedLength){
        if(isEmpty())return true;
        if(first.getStartStringIndex()!=0){
            System.err.println("First token starts at " + first.getStartStringIndex());
            System.err.println(this);
            return false;
        }
        for(Node curr=first.next;curr!=null;curr=curr.next){
            if(curr.getStartStringIndex()!=curr.prev.getEndStringIndex()){
                System.err.println("Token "+curr.prev.token+" ends at "+curr.prev.getEndStringIndex()+" while token "+curr.token+" starts at "+curr.getStartStringIndex());
                System.err.println(this);
                return false;
            }
        }
        if(expectedLength!=-1 && last.getEndStringIndex()!= expectedLength){
            System.err.println("Last token end at "+last.getEndStringIndex()+" while text is of size "+expectedLength);
            System.err.println(this);
            return false;
        }
        return true;
    }

    private Node calcFormat(Node node){
        if(isEmpty())return node;
        int offset=node.getPrev()!=null?node.getPrev().getEndStringIndex():0;
        for(Node curr=node;curr!=null;curr=curr.next){
            curr.strIndex=offset;
            offset+=curr.getToken().getLength();
        }
        checkFormat(-1);
        return node;
    }

    public TokenList subList(int start,int end){
        return new TokenList(getNode(start),getNode(end),this);
    }
    public TokenList subList(Token start,Token end){
        return new TokenList(getNode(start),getNode(end),this);
    }
    public TokenList subList(Node start,Node end){
        return new TokenList(start,end,this);
    }
    @Override
    public String toString() {
        if(isEmpty())return "";
        if(first==last)return "["+first.getToken()+"]";
        StringBuilder builder = new StringBuilder("[");
        for (Node curr = first; curr != last; curr = curr.getNext()){
            builder.append(curr.getToken());
            builder.append(" ,");
        }
        builder.append(last.getToken());
        builder.append("]");
        return builder.toString();
    }

}
