package com.skinny121.mcide;

import com.skinny121.mcide.tree.ASTNode;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 30/03/14
 * Time: 9:38 AM
 *
 * Interface that defines a file type is will be used by the editor.
 *
 */
public interface FileType {

    /**
     * Whether the given file name is of this file type.
     * @param name filename
     * @return if the given filename is of this file type.
     */
    boolean accept(String name);

    /**
     * Creates a new lexer for the given text.
     * @param text text that you want to create the lexer for
     * @return the lexer
     */
    Lexer lex(String text);

    ASTNode parse(Lexer tokens);

    /**
     * Decodes the input stream into a string that will be displayed in the editor.
     * @param in Contains the information that will be decoded.
     * @return Decoded string
     * @throws IOException
     */
    String decode(InputStream in) throws IOException;

    /**
     * Encodes a string into a given file format.
     * @param text String to be encoded
     * @param out Output stream that encoded data will be written to
     * @throws IOException
     */
    void encode(String text,OutputStream out) throws IOException;


    void setErrorHandler(ErrorHandler errorHandler);
}
