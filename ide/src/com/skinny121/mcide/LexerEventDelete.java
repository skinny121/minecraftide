package com.skinny121.mcide;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 30/03/14
 * Time: 2:43 PM
 */
public class LexerEventDelete {
    private List<Token> oldTokens;
    private List<Token> newTokens;

    public LexerEventDelete(List<Token> oldTokens, List<Token> newTokens) {
        this.oldTokens = oldTokens;
        this.newTokens = newTokens;
    }

}
