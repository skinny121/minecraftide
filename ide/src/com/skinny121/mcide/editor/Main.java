package com.skinny121.mcide.editor;

import com.skinny121.mcide.impl.json.JsonFileType;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventType;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 27/03/14
 * Time: 9:29 PM
 */
public class Main extends Application {
    private Pane pane;
    private VBox menu;
    @Override
    public void start(Stage stage) throws Exception {

        pane=new StackPane();
        stage.setTitle("NBT Editor");
        stage.setScene(new Scene(pane, 200, 200));
        stage.getScene().getStylesheets().add("style.css");
        initHome();
        setPage(menu);


        stage.show();
    }

    private void setPage(Node node){
        pane.getChildren().clear();
        pane.getChildren().add(node);
    }

    private void initHome(){
        menu=new VBox();
        menu.setId("menu");

        Text title=new Text("MinecraftIDE");
        title.setId("menu-title");
        menu.getChildren().add(title);

        Button button=new Button("Json Editor");
        button.setId("menu-json");
        button.setOnAction(this::buttonPress);

        menu.getChildren().add(button);
    }

    private void buttonPress(ActionEvent event){
        setPage(new Editor(JsonFileType.getInstance()));
    }


    public static void main(String[] args){
        launch(args);
    }
}
