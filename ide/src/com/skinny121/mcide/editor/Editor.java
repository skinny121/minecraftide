package com.skinny121.mcide.editor;

import com.skinny121.mcide.FileType;
import com.skinny121.mcide.Lexer;
import com.skinny121.mcide.tree.ASTNode;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.StyleSpansBuilder;

import java.util.Collection;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 27/03/14
 * Time: 9:38 PM
 */
public class Editor extends CodeArea {

    private static final Collection<String> DEFAULT_STYLE=Collections.singleton("default");
    private static final Collection<String> NUMBER_STYLE=Collections.singleton("number");
    private ContextMenu suggestionMenu;

    private FileType fileType;
    private Lexer lexer;
    private ASTNode tree;
    protected Editor(FileType fileType) {
        super();
        this.fileType=fileType;
        this.lexer=fileType.lex("");
        getStyleClass().add("editor");
        plainTextChanges().monitor(Throwable::printStackTrace);
        plainTextChanges().subscribe(e->{
            if(!e.getRemoved().isEmpty())lexer.removeText(e.getPosition(),e.getRemoved().length());
            if(!e.getInserted().isEmpty())lexer.insertText(e.getInserted(),e.getPosition());
            this.tree = this.fileType.parse(lexer);
            this.formatText();
        });
    }

    private void formatText(){
        this.setStyleSpans(0,this.tree);
    }


//    private void changedCaretPosition(){
//        int pos=getCaretPosition()-1;
//        if(pos<0)return;
//        if(getText().charAt(pos)=='.'){
//            if(getContextMenu()==null){
//                System.out.println("showing");
//                StyledTextAreaSkin skin=((StyledTextAreaSkin) getSkin());
//                double x=skin.getCaretOffsetX();
//                double y=skin.getCaretOffsetY()+skin.getLineHeight()+10;
//                Point2D screenCords=localToScreen(x,y);
//                System.out.println(x+" "+y);
//                suggestionMenu.show(this, screenCords.getX(), screenCords.getY());
//            }
//        }
//    }

}
