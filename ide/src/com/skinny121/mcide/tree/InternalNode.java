package com.skinny121.mcide.tree;

import com.skinny121.mcide.TokenContext;
import org.fxmisc.richtext.StyleSpan;
import org.fxmisc.richtext.StyleSpans;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 7/12/14
 * Time: 10:00 AM
 *
 * An internal node has one leaf for every string index/
 */
public abstract class InternalNode extends ASTNode{
    protected InternalNode(TokenContext tokenContext) {
        super(tokenContext);
    }

    @Override
    public int getSpanCount() {
        return getChildren().mapToInt(StyleSpans::getSpanCount).sum();
    }

    @Override
    public StyleSpan<Collection<String>> getStyleSpan(int index){
        for (ASTNode child : getChildren().toArray(ASTNode[]::new)) {
            int size = child.getSpanCount();
            if (index < size) {
                return child.getStyleSpan(index);
            }
            index -= size;
        }
        throw new IndexOutOfBoundsException();
    }

    @Override
    public Stream<Collection<String>> styleStream() {
        return getChildren().flatMap(StyleSpans::styleStream);
    }

    @Override
    public Stream<StyleSpan<Collection<String>>> stream() {
        return getChildren().flatMap(StyleSpans::stream);
    }

    @Override
    public Iterator<StyleSpan<Collection<String>>> iterator() {
        return stream().iterator();
    }
}
