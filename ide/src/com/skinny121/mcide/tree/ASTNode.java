package com.skinny121.mcide.tree;

import com.skinny121.mcide.Node;
import com.skinny121.mcide.TokenContext;
import org.fxmisc.richtext.StyleSpans;
import org.fxmisc.richtext.TwoLevelNavigator;

import java.util.Collection;
import java.util.stream.Stream;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 30/03/14
 * Time: 9:42 AM
 */
public abstract class ASTNode implements StyleSpans<Collection<String>>{
    private ASTNode parent;
    protected TokenContext tokenContext;

    protected ASTNode(TokenContext tokenContext){
        this.tokenContext=tokenContext;
    }

    public abstract Stream<? extends ASTNode> getChildren();

    public void setParent(ASTNode parent){
        this.parent=parent;
    }

    public ASTNode getParent(){
        return parent;
    }

    public Node getStartNode(){
        return tokenContext.startNode();
    }

    public Node getEndNode(){
        return tokenContext.endNode();
    }

    public int getStartStringIndex(){
        return getStartNode().getStartStringIndex();
    }

    public int getEndStringIndex(){
        return getEndNode().getEndStringIndex();
    }

    public int getStringLength(){
        return getEndStringIndex()-getStartStringIndex();
    }

    @Override
    public int length() {
        return tokenContext.length();
    }

    public String getStyle(){
        return getParent().getStyle();
    }

    private TwoLevelNavigator navigator = new TwoLevelNavigator(
            this::getSpanCount,
            i->getStyleSpan(i).getLength());

    @Override
    public Position position(int major, int minor) {
        return navigator.position(major, minor);
    }

    @Override
    public Position offsetToPosition(int offset, Bias bias) {
        return navigator.offsetToPosition(offset, bias);
    }

}
