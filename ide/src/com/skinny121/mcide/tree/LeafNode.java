package com.skinny121.mcide.tree;

import com.skinny121.mcide.TokenContext;
import org.fxmisc.richtext.StyleSpan;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * Created with IntelliJ IDEA.
 * User: BenLewis
 * Date: 7/12/14
 * Time: 10:05 AM
 */
public abstract class LeafNode extends ASTNode {
    protected LeafNode(TokenContext tokenContext) {
        super(tokenContext);
    }

    @Override
    public Stream<? extends ASTNode> getChildren() {
        return Stream.empty();
    }

    @Override
    public void forEach(Consumer<? super StyleSpan<Collection<String>>> action) {
        action.accept(getStyleSpan(0));
    }

    @Override
    public Iterator<StyleSpan<Collection<String>>> iterator() {
        return new Iterator<StyleSpan<Collection<String>>>() {
            private boolean flag = true;
            @Override
            public boolean hasNext() {
                return flag;
            }

            @Override
            public StyleSpan<Collection<String>> next() {
                if(hasNext()){
                    flag = false;
                    return getStyleSpan(0);
                }else{
                    throw new NoSuchElementException();
                }
            }
        };
    }

    @Override
    public Stream<Collection<String>> styleStream() {
        return Stream.of(Collections.singleton(getStyle()));
    }

    @Override
    public Stream<StyleSpan<Collection<String>>> stream() {
        return Stream.of(getStyleSpan(0));
    }

    @Override
    public int getSpanCount() {
        return 1;
    }

    @Override
    public StyleSpan<Collection<String>> getStyleSpan(int index) {
        if(index!=0){
            throw new IndexOutOfBoundsException("Leaf can only have style span at index 0");
        }
        return new StyleSpan<>(Collections.singleton(getStyle()), length());
    }
}
